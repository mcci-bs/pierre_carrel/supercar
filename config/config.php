<!--
 Ce répertoire contient les fichiers de configuration de votre application.
config.php contient des informations de configuration telles que les informations
de connexion à la base de données.
 -->


<?php
// Informations de connexion à la base de données
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASSWORD', '');
define('DB_NAME', 'supercar');

// Autres configurations
define('BASE_URL', 'http://localhost/supercar/');
define('JWT_SECRET_KEY', 'ma_clef_secrète_pour_la_génération_de_jwt');
?>
