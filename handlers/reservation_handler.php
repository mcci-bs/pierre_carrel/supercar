<?php

require_once '../controllers/ReservationController.php';

$carId = $_POST['car_id'];
$userId = $_POST['user_id'];
$reservationDate = $_POST['reservation_date'];
$startTime = $_POST['reservation_start_time'];
$endTime = $_POST['reservation_end_time'];


// Vérification de la date de réservation
$currentDate = date('Y-m-d');
if ($reservationDate < $currentDate) {
    // Si la date de réservation est inférieure à la date actuelle, renvoyer une erreur
    header("Location: reservation.php?car_id=" . urlencode($carId) . "&user_id=" . urlencode($userId) . "&success=false&error=date");
    exit();
}

// Vérification de l'heure de debut et de fin de réservation
$currentDateTime = date('Y-m-d H:i:s');
if ($reservationDate === $currentDate && $startTime < date('H:i:s')) {
    // Si l'heure de réservation est inférieure à l'heure actuelle pour la même date, renvoyer une erreur
    header("Location: reservation.php?car_id=" . urlencode($carId) . "&user_id=" . urlencode($userId) . "&success=false&error=starttime");
    exit();
}

$currentDateTime = date('Y-m-d H:i:s');
if ($reservationDate === $currentDate && $endTime < date('H:i:s')) {
    // Si l'heure de réservation est inférieure à l'heure actuelle pour la même date, renvoyer une erreur
    header("Location: reservation.php?car_id=" . urlencode($carId) . "&user_id=" . urlencode($userId) . "&success=false&error=endtime");
    exit();
}


if ($reservationDate === $currentDate && ($startTime < date('H:i:s') && $endTime < date('H:i:s') || $endTime <= $startTime)) {
    // Si l'heure de réservation est inférieure à l'heure actuelle pour la même date
    // ou si l'heure de fin est inférieure ou égale à l'heure de début,
    // renvoyer une erreur
    header("Location: reservation.php?car_id=" . urlencode($carId) . "&user_id=" . urlencode($userId) . "&success=false&error=timeerror");
    exit();
}


//Appel à la méthode de réservation du contrôleur
$success = ReservationController::makeReservation($userId,$carId, $reservationDate, $startTime, $endTime);

if ($success) {
    // Si la réservation réussit, rediriger avec les paramètres GET
    header("Location: reservation.php?car_id=" . urlencode($carId) . "&user_id=" . urlencode($userId) . "&success=true");

    exit();
} else {
    // Si la réservation échoue, rediriger avec les paramètres GET
    header("Location: reservation.php?car_id=" . urlencode($carId) . "&user_id=" . urlencode($userId) . "&success=false");

    exit();
}
?>