<?php
require_once '../controllers/CarController.php';
require_once '../utils/CaracteristiqueEnum.php';

$car_id = $_POST['car_id'];
$marque = $_POST['marque'];
$model = $_POST['model'];
$year = $_POST['year'];
$description = $_POST['description'];
$color = $_POST['color'];
$price = $_POST['price'];
$consumption = $_POST['consumption'];
$caracteristiques = $_POST['caracteristiques'];
$images = $_FILES['images'];
$oldImages= $_POST['oldImages'];
// Appel de la méthode de mise a jour des voiture avec leurs images
$success = CarController::updateCarAndImages($car_id, $marque, $model, $year, $description, $color, $price, $consumption, $caracteristiques, $images,$oldImages);


if ($success) {
    // Rediriger l'utilisateur vers la page de connexion en cas de succès
    header("Location: home.php");
    exit();
} else {
    // Afficher un message d'erreur si l'inscription a échoué
    $errorMessage = "Erreur lors de la modification. Veuillez réessayer.";
}
?>
