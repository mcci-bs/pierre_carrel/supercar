<?php
require_once '../controllers/AuthController.php';

$username = $_POST['username'];
$password = $_POST['password'];
$firstName = $_POST['firstName'];
$lastName = $_POST['lastName'];
$email = $_POST['email'];
$phone = $_POST['phone'];

// Appeler la méthode d'inscription de l'AuthController
$success = AuthController::registerUser($username, $password, $firstName, $lastName, $email, $phone);

if ($success) {
    // Rediriger l'utilisateur vers la page de connexion en cas de succès
    header("Location: login.php");
    exit();
} else {
    // Afficher un message d'erreur si l'inscription a échoué
    $errorMessage = "Erreur lors de l'inscription. Veuillez réessayer.";
}
?>
