<?php

require_once '../controllers/AuthController.php';

$username = $_POST['username'];
$password = $_POST['password'];

// Appeler la méthode d'authentification de l'AuthController
$token = AuthController::authenticate($username, $password);

if ($token) {
    // Stocker le token dans une session ou un cookie si nécessaire
    session_start();
    $_SESSION['token'] = $token;

    // Rediriger l'utilisateur vers la page d'accueil ou une autre page protégée
    header("Location: home.php");
    exit();
} else {
    // Afficher un message d'erreur si l'authentification a échoué
    $errorMessage = "Nom d'utilisateur ou mot de passe incorrect.";
}
?>
