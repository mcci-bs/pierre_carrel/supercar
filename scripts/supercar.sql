-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : jeu. 18 avr. 2024 à 18:50
-- Version du serveur : 10.4.27-MariaDB
-- Version de PHP : 7.4.33

SET
SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET
time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `supercar`
--

-- --------------------------------------------------------

--
-- Structure de la table `caracteristique`
--

CREATE TABLE `caracteristique`
(
    `id`         int(11) NOT NULL,
    `voiture_id` int(11) NOT NULL,
    `libelle`    varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `compte`
--

CREATE TABLE `compte`
(
    `id`       int(11) NOT NULL,
    `user_id`  bigint(25) NOT NULL,
    `password` varchar(255) NOT NULL,
    `username` varchar(45) DEFAULT NULL,
    `role`     varchar(6)  DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `compte`
--

INSERT INTO `compte` (`id`, `user_id`, `password`, `username`, `role`)
VALUES (1, 1, '$2y$10$stirw..07XS/7UQcjJ7zquGeOO0BjrTFhvAYj7MHe65kx6HNLMw86', 'Shelby', 'admin'),
       (2, 2, '$2y$10$Ul05GFyxvdUFCO2VMompXO7RG0kF89AKogWReF.RGCdtP/Ghs93bu', 'Kamal', 'client'),
       (3, 3, '$2y$10$/.aaeLP8O0A1lqZSQ2FVoOOZ1cjhnaKEr/isB1AjdRnTHrHp16MQG', 'Pierre21', 'admin');

-- --------------------------------------------------------

--
-- Structure de la table `image`
--

CREATE TABLE `image`
(
    `id`         int(11) NOT NULL,
    `voiture_id` int(11) NOT NULL,
    `url`        varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `reservation`
--

CREATE TABLE `reservation`
(
    `id`                      int(11) NOT NULL,
    `utilisateur_id`          bigint(25) NOT NULL,
    `voiture_id`              int(11) NOT NULL,
    `date_reservation`        date         DEFAULT NULL,
    `heure_debut_reservation` time         DEFAULT NULL,
    `heure_fin_reservation`   time         DEFAULT NULL,
    `statut`                  varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur`
(
    `id`        bigint(25) NOT NULL,
    `nom`       varchar(255) NOT NULL,
    `prenom`    varchar(255) NOT NULL,
    `email`     varchar(255) NOT NULL,
    `telephone` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `nom`, `prenom`, `email`, `telephone`)
VALUES (1, 'Noah', 'Patrick', 'kshelby@domaine.com', '123456789'),
       (2, 'jamal123', 'jamal', 'jamal@domaine.com', '58311166'),
       (3, 'Fotso', 'Pierre', 'karrelfotso@gmail.com', '58311155');

-- --------------------------------------------------------

--
-- Structure de la table `voiture`
--

CREATE TABLE `voiture`
(
    `id`           int(11) NOT NULL,
    `marque`       varchar(255) DEFAULT NULL,
    `modele`       varchar(255) DEFAULT NULL,
    `annee`        int(11) DEFAULT NULL,
    `description`  text         DEFAULT NULL,
    `couleur`      varchar(45)  DEFAULT NULL,
    `prix`         varchar(45)  DEFAULT NULL,
    `consommation` varchar(45)  DEFAULT NULL,
    `pays`         varchar(45)  DEFAULT NULL,
    `disponible`   tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `caracteristique`
--
ALTER TABLE `caracteristique`
    ADD PRIMARY KEY (`id`),
  ADD KEY `fk_caracteristique_voiture1_idx` (`voiture_id`);

--
-- Index pour la table `compte`
--
ALTER TABLE `compte`
    ADD PRIMARY KEY (`id`),
  ADD KEY `fk_account_user_idx` (`user_id`);

--
-- Index pour la table `image`
--
ALTER TABLE `image`
    ADD PRIMARY KEY (`id`),
  ADD KEY `fk_image_voiture1_idx` (`voiture_id`);

--
-- Index pour la table `reservation`
--
ALTER TABLE `reservation`
    ADD PRIMARY KEY (`id`, `utilisateur_id`, `voiture_id`),
  ADD KEY `fk_utilisateur_has_voiture_voiture1_idx` (`voiture_id`),
  ADD KEY `fk_utilisateur_has_voiture_utilisateur1_idx` (`utilisateur_id`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
    ADD PRIMARY KEY (`id`);

--
-- Index pour la table `voiture`
--
ALTER TABLE `voiture`
    ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `caracteristique`
--
ALTER TABLE `caracteristique`
    MODIFY `id` int (11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `compte`
--
ALTER TABLE `compte`
    MODIFY `id` int (11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `image`
--
ALTER TABLE `image`
    MODIFY `id` int (11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
    MODIFY `id` bigint(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `voiture`
--
ALTER TABLE `voiture`
    MODIFY `id` int (11) NOT NULL AUTO_INCREMENT;


--
-- AUTO_INCREMENT pour la table `reservation`
--
ALTER TABLE `reservation`
    MODIFY `id` int (11) NOT NULL AUTO_INCREMENT;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `caracteristique`
--
ALTER TABLE `caracteristique`
    ADD CONSTRAINT `fk_caracteristique_voiture1` FOREIGN KEY (`voiture_id`) REFERENCES `voiture` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `image`
--
ALTER TABLE `image`
    ADD CONSTRAINT `fk_image_voiture1` FOREIGN KEY (`voiture_id`) REFERENCES `voiture` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `compte`
--
ALTER TABLE `compte`
    ADD CONSTRAINT `fk_account_user` FOREIGN KEY (`user_id`) REFERENCES `utilisateur` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `reservation`
--
ALTER TABLE `reservation`
    ADD CONSTRAINT `fk_utilisateur_has_voiture_utilisateur1` FOREIGN KEY (`utilisateur_id`) REFERENCES `utilisateur` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_utilisateur_has_voiture_voiture1` FOREIGN KEY (`voiture_id`) REFERENCES `voiture` (`id`) ON
DELETE
NO ACTION ON
UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

INSERT INTO `voiture` (`id`, `marque`, `modele`, `annee`, `description`, `couleur`, `prix`, `consommation`, `pays`, `disponible`) VALUES
                                                                                                                                      (1, 'Toyota', 'Corolla', 2022, NULL, 'bleu', '25000', 'Essence', 'Japon', 1),
                                                                                                                                      (2, 'Ford', 'Mustang', 2023, NULL, 'rouge', '45000', 'Essence', 'États-Unis', 1),
                                                                                                                                      (3, 'Honda', 'Civic', 2021, NULL, 'argent', '22000', 'Essence', 'Japon', 1),
                                                                                                                                      (4, 'Volkswagen', 'Golf', 2022, NULL, 'noir', '27000', 'Essence', 'Allemagne', 1),
                                                                                                                                      (5, 'Chevrolet', 'Camaro', 2023, NULL, 'jaune', '47000', 'Essence', 'États-Unis', 1),
                                                                                                                                      (6, 'Nissan', 'Altima', 2022, NULL, 'blanc', '28000', 'Essence', 'Japon', 1),
                                                                                                                                      (7, 'BMW', '3 Series', 2022, NULL, 'gris', '40000', 'Essence', 'Allemagne', 1),
                                                                                                                                      (8, 'Mercedes-Benz', 'C-Class', 2023, NULL, 'argent', '45000', 'Essence', 'Allemagne', 1),
                                                                                                                                      (9, 'Audi', 'A4', 2022, NULL, 'bleu', '42000', 'Essence', 'Allemagne', 1),
                                                                                                                                      (10, 'Lexus', 'ES', 2023, NULL, 'rouge', '50000', 'Essence', 'Japon', 1),
                                                                                                                                      (11, 'Subaru', 'Outback', 2021, NULL, 'bleu', '35000', 'Essence', 'Japon', 1),
                                                                                                                                      (12, 'Mazda', 'CX-5', 2023, NULL, 'noir', '32000', '', 'Japon', 1),
                                                                                                                                      (13, 'Hyundai', 'Sonata', 2022, NULL, 'argent', '30000', 'Essence', 'Corée du Sud', 1),
                                                                                                                                      (14, 'Kia', 'Optima', 2023, NULL, 'gris', '28000', 'Essence', 'Corée du Sud', 1),
                                                                                                                                      (15, 'Volvo', 'S60', 2022, NULL, 'bleu', '40000', 'Essence', 'Suède', 1),
                                                                                                                                      (16, 'Tesla', 'Model 3', 2023, NULL, 'argent', '55000', 'Électrique', 'États-Unis', 1),
                                                                                                                                      (17, 'Porsche', '911', 2022, NULL, 'rouge', '90000', 'Essence', 'Allemagne', 1),
                                                                                                                                      (18, 'Renault', 'Clio', 2022, NULL, 'gris', '20000', 'Essence', 'France', 1),
                                                                                                                                      (19, 'Peugeot', '308', 2023, NULL, 'blanc', '22000', 'Essence', 'France', 1),
                                                                                                                                      (20, 'Citroën', 'C4', 2022, NULL, 'rouge', '21000', 'Essence', 'France', 1),
                                                                                                                                      (21, 'Fiat', '500', 2023, NULL, 'jaune', '18000', 'Essence', 'Italie', 1),
                                                                                                                                      (22, 'Alfa Romeo', 'Giulia', 2022, NULL, 'noir', '40000', 'Essence', 'Italie', 1),
                                                                                                                                      (23, 'Maserati', 'Ghibli', 2023, NULL, 'bleu', '80000', 'Essence', 'Italie', 1),
                                                                                                                                      (24, 'Lamborghini', 'Aventador', 2022, NULL, 'rouge', '300000', 'Essence', 'Italie', 1),
                                                                                                                                      (25, 'Ferrari', '488 GTB', 2023, NULL, 'jaune', '250000', 'Essence', 'Italie', 1),
                                                                                                                                      (26, 'Bugatti', 'Chiron', 2022, NULL, 'noir', '3000000', 'Essence', 'France', 1),
                                                                                                                                      (27, 'Bentley', 'Continental GT', 2023, NULL, 'blanc', '250000', 'Essence', 'Royaume-Uni', 1),
                                                                                                                                      (28, 'Rolls-Royce', 'Phantom', 2022, NULL, 'noir', '450000', 'Essence', 'Royaume-Uni', 1),
                                                                                                                                      (29, 'Jaguar', 'XF', 2023, NULL, 'rouge', '60000', 'Essence', 'Royaume-Uni', 1),
                                                                                                                                      (30, 'Land Rover', 'Range Rover', 2022, NULL, 'noir', '100000', 'Essence', 'Royaume-Uni', 1),
                                                                                                                                      (31, 'McLaren', '720S', 2023, NULL, 'bleu', '300000', 'Essence', 'Royaume-Uni', 1),
                                                                                                                                      (32, 'Aston Martin', 'DB11', 2022, NULL, 'argent', '250000', 'Essence', 'Royaume-Uni', 1),
                                                                                                                                      (33, 'Lotus', 'Evora', 2023, NULL, 'vert', '80000', 'Essence', 'Royaume-Uni', 1),
                                                                                                                                      (34, 'Mini', 'Cooper', 2022, NULL, 'rouge', '25000', 'Essence', 'Royaume-Uni', 1),
                                                                                                                                      (35, 'Alpine', 'A110', 2023, NULL, 'bleu', '60000', 'Essence', 'France', 1),
                                                                                                                                      (36, 'Volvo', 'XC90', 2022, NULL, 'blanc', '70000', 'Essence', 'Suède', 1),
                                                                                                                                      (37, 'Saab', '9-5', 2023, NULL, 'argent', '35000', 'Essence', 'Suède', 1),
                                                                                                                                      (38, 'Koenigsegg', 'Regera', 2022, NULL, 'orange', '2000000', 'Essence', 'Suède', 1),
                                                                                                                                      (39, 'Volvo', 'V60', 2023, NULL, 'rouge', '45000', 'Essence', 'Suède', 1),
                                                                                                                                      (40, 'Polestar', '2', 2022, NULL, 'noir', '60000', 'Électrique', 'Suède', 1),
                                                                                                                                      (41, 'Škoda', 'Octavia', 2023, NULL, 'bleu', '30000', 'Essence', 'République Tchèque', 1),
                                                                                                                                      (42, 'Dacia', 'Duster', 2022, NULL, 'gris', '20000', 'Essence', 'Roumanie', 1),
                                                                                                                                      (43, 'Lada', 'Niva', 2023, NULL, 'vert', '15000', 'Essence', 'Russie', 1),
                                                                                                                                      (44, 'Lada', 'Granta', 2022, NULL, 'blanc', '10000', 'Essence', 'Russie', 1),
                                                                                                                                      (45, 'Geely', 'Emgrand 7', 2023, NULL, 'bleu', '18000', 'Essence', 'Chine', 1),
                                                                                                                                      (46, 'BYD', 'Tang', 2022, NULL, 'noir', '25000', 'Électrique', 'Chine', 1),
                                                                                                                                      (47, 'Chery', 'Tiggo 7', 2023, NULL, 'rouge', '20000', 'Essence', 'Chine', 1),
                                                                                                                                      (48, 'Great Wall', 'Haval H6', 2022, NULL, 'gris', '22000', 'Essence', 'Chine', 1),
                                                                                                                                      (49, 'NIO', 'ES8', 2023, NULL, 'blanc', '60000', 'Électrique', 'Chine', 1),
                                                                                                                                      (50, 'MG', 'ZS', 2022, NULL, 'argent', '19000', 'Essence', 'Chine', 1),
                                                                                                                                      (51, 'Hongqi', 'HS5', 2023, NULL, 'rouge', '35000', 'Essence', 'Chine', 1);

INSERT INTO `caracteristique` (`id`, `voiture_id`, `libelle`)
VALUES (1, 1, 'fiable'),
       (2, 1, 'économique'),
       (3, 1, 'spacieuse'),
       (4, 2, 'sportive'),
       (5, 2, 'puissante'),
       (6, 2, 'luxe'),
       (7, 3, 'compacte'),
       (8, 3, 'économe'),
       (9, 3, 'fiable'),
       (10, 4, 'sobre'),
       (11, 4, 'confortable'),
       (12, 4, 'polyvalente'),
       (13, 5, 'puissante'),
       (14, 5, 'sportive'),
       (15, 5, 'impressionnante'),
       (16, 6, 'élégante'),
       (17, 6, 'confortable'),
       (18, 6, 'spacieuse'),
       (19, 7, 'luxueuse'),
       (20, 7, 'performante'),
       (21, 7, 'élégante'),
       (22, 8, 'élégante'),
       (23, 8, 'confortable'),
       (24, 8, 'performante'),
       (25, 9, 'technologique'),
       (26, 9, 'confortable'),
       (27, 9, 'sécuritaire'),
       (28, 10, 'luxe'),
       (29, 10, 'spacieuse'),
       (30, 10, 'confortable'),
       (31, 11, 'robuste'),
       (32, 11, 'tout-terrain'),
       (33, 11, 'confortable'),
       (34, 12, 'dynamique'),
       (35, 12, 'agile'),
       (36, 12, 'confortable'),
       (37, 13, 'sécuritaire'),
       (38, 13, 'économique'),
       (39, 13, 'confortable'),
       (40, 14, 'élégante'),
       (41, 14, 'confortable'),
       (42, 14, 'économique'),
       (43, 15, 'sécuritaire'),
       (44, 15, 'confortable'),
       (45, 15, 'innovante'),
       (46, 16, 'électrique'),
       (47, 16, 'autonome'),
       (48, 16, 'innovante'),
       (49, 17, 'sportive'),
       (50, 17, 'luxe'),
       (51, 17, 'performante'),
       (52, 18, 'compacte'),
       (53, 18, 'stylée'),
       (54, 18, 'amusante'),
       (55, 19, 'sportive'),
       (56, 19, 'légère'),
       (57, 19, 'agile'),
       (58, 20, 'électrique'),
       (59, 20, 'performante'),
       (60, 20, 'technologique'),
       (61, 21, 'spacieuse'),
       (62, 21, 'économique'),
       (63, 21, 'fiable'),
       (64, 22, 'robuste'),
       (65, 22, 'économique'),
       (66, 22, 'spacieux'),
       (67, 23, 'tout-terrain'),
       (68, 23, 'simple'),
       (69, 23, 'abordable'),
       (70, 24, 'basique'),
       (71, 24, 'économique'),
       (72, 24, 'pratique'),
       (73, 25, 'économique'),
       (74, 25, 'fiable'),
       (75, 25, 'spacieuse'),
       (76, 26, 'électrique'),
       (77, 26, 'performante'),
       (78, 26, 'technologique'),
       (79, 27, 'compacte'),
       (80, 27, 'confortable'),
       (81, 27, 'élégante'),
       (82, 28, 'robuste'),
       (83, 28, 'tout-terrain'),
       (84, 28, 'spacieuse'),
       (85, 29, 'électrique'),
       (86, 29, 'luxe'),
       (87, 29, 'performante'),
       (88, 30, 'compacte'),
       (89, 30, 'économique'),
       (90, 30, 'technologique'),
       (91, 31, 'luxueuse'),
       (92, 31, 'spacieuse'),
       (93, 31, 'confortable'),
       (94, 32, 'sportive'),
       (95, 32, 'luxe'),
       (96, 32, 'prestigieuse'),
       (97, 33, 'extravagante'),
       (98, 33, 'puissante'),
       (99, 33, 'légendaire'),
       (100, 34, 'sportive'),
       (101, 34, 'luxe'),
       (102, 34, 'prestigieuse'),
       (103, 35, 'exclusivité'),
       (104, 35, 'ultra-rapide'),
       (105, 35, 'luxe extrême'),
       (106, 36, 'luxe'),
       (107, 36, 'puissance'),
       (108, 36, 'confort'),
       (109, 37, 'grand luxe'),
       (110, 37, 'prestigieuse'),
       (111, 38, 'élégance'),
       (112, 38, 'puissance'),
       (113, 38, 'confort'),
       (114, 39, 'luxueuse'),
       (115, 39, 'tout-terrain'),
       (116, 39, 'spacieuse'),
       (117, 40, 'hautes performances'),
       (118, 40, 'technologie avancée'),
       (119, 40, 'légère'),
       (120, 41, 'élégante'),
       (121, 41, 'puissante'),
       (122, 41, 'grand tourisme'),
       (123, 42, 'sportive'),
       (124, 42, 'légère'),
       (125, 42, 'agile'),
       (126, 43, 'compacte'),
       (127, 43, 'stylée'),
       (128, 43, 'amusante'),
       (129, 44, 'électrique'),
       (130, 44, 'performante'),
       (131, 44, 'technologique'),
       (132, 45, 'sportive'),
       (133, 45, 'légère'),
       (134, 45, 'agile'),
       (135, 46, 'sécuritaire'),
       (136, 46, 'spacieuse'),
       (137, 46, 'luxe'),
       (138, 47, 'confortable'),
       (139, 47, 'sobre'),
       (140, 47, 'élégante'),
       (141, 48, 'compacte'),
       (142, 48, 'économique'),
       (143, 48, 'colorée'),
       (144, 49, 'élégante'),
       (145, 49, 'sportive'),
       (146, 49, 'performante'),
       (147, 50, 'luxueuse'),
       (148, 50, 'puissante'),
       (149, 50, 'prestigieuse'),
       (150, 51, 'extravagante'),
       (151, 51, 'puissante'),
       (152, 51, 'légendaire'),
       (153, 50, 'sportive'),
       (154, 21, 'luxe'),
       (155, 18, 'prestigieuse'),
       (156, 51, 'exclusivité'),
       (157, 23, 'ultra-rapide'),
       (158, 25, 'luxe extrême'),
       (159, 18, 'luxe'),
       (160, 4, 'puissance'),
       (161, 5, 'confort'),
       (162, 2, 'confort'),
       (163, 20, 'sportive'),
       (164, 41, 'luxe'),
       (165, 43, 'prestigieuse'),
       (166, 40, 'compacte'),
       (167, 20, 'stylée'),
       (168, 30, 'amusante'),
       (169, 39, 'électrique'),
       (170, 1, 'performante'),
       (171, 40, 'technologique'),
       (172, 46, 'sportive'),
       (173, 5, 'légère'),
       (174, 42, 'agile'),
       (175, 42, 'sécuritaire'),
       (176, 31, 'spacieuse'),
       (177, 29, 'luxe'),
       (178, 2, 'confortable'),
       (179, 22, 'sobre'),
       (180, 3, 'élégante'),
       (181, 49, 'compacte'),
       (182, 32, 'économique'),
       (183, 15, 'colorée'),
       (184, 27, 'élégante'),
       (185, 38, 'sportive'),
       (186, 6, 'performante'),
       (187, 20, 'luxueuse'),
       (188, 31, 'puissante'),
       (189, 42, 'prestigieuse'),
       (190, 16, 'extravagante'),
       (191, 5, 'puissante'),
       (192, 27, 'légendaire'),
       (193, 18, 'sportive'),
       (194, 8, 'luxe'),
       (195, 35, 'prestigieuse'),
       (196, 50, 'exclusivité'),
       (197, 44, 'ultra-rapide'),
       (198, 17, 'luxe extrême'),
       (199, 4, 'luxe'),
       (200, 17, 'puissance'),
       (201, 24, 'confort'),
       (202, 18, 'grand luxe'),
       (203, 16, 'prestigieuse'),
       (204, 27, 'incomparable'),
       (205, 37, 'élégante'),
       (206, 50, 'puissante'),
       (207, 38, 'confort'),
       (208, 39, 'sportive'),
       (209, 31, 'luxe'),
       (210, 37, 'prestigieuse'),
       (211, 41, 'compacte'),
       (212, 43, 'stylée'),
       (213, 38, 'amusante'),
       (214, 12, 'électrique'),
       (215, 47, 'performante'),
       (216, 47, 'technologique'),
       (217, 40, 'sportive'),
       (218, 9, 'légère'),
       (219, 26, 'agile'),
       (220, 50, 'sécuritaire'),
       (221, 19, 'spacieuse'),
       (222, 48, 'luxe'),
       (223, 29, 'confortable'),
       (224, 50, 'sobre'),
       (225, 12, 'élégante'),
       (226, 10, 'compacte'),
       (227, 13, 'économique'),
       (228, 37, 'colorée'),
       (229, 41, 'élégante'),
       (230, 45, 'sportive'),
       (231, 48, 'performante'),
       (232, 2, 'luxueuse'),
       (233, 21, 'puissante'),
       (234, 46, 'prestigieuse'),
       (235, 15, 'extravagante'),
       (236, 38, 'puissante'),
       (237, 44, 'légendaire'),
       (238, 1, 'sportive'),
       (239, 27, 'luxe'),
       (240, 28, 'prestigieuse'),
       (241, 10, 'exclusivité'),
       (242, 14, 'ultra-rapide'),
       (243, 41, 'luxe extrême'),
       (244, 11, 'luxe'),
       (245, 32, 'puissance'),
       (246, 26, 'confort'),
       (247, 34, 'grand luxe'),
       (248, 39, 'prestigieuse'),
       (249, 44, 'incomparable'),
       (250, 48, 'élégante'),
       (251, 9, 'puissante'),
       (252, 1, 'confort'),
       (253, 29, 'sportive'),
       (254, 40, 'luxe'),
       (255, 10, 'prestigieuse'),
       (256, 32, 'compacte'),
       (257, 29, 'stylée'),
       (258, 46, 'amusante'),
       (259, 41, 'électrique'),
       (260, 14, 'performante'),
       (261, 50, 'technologique'),
       (262, 4, 'sportive'),
       (263, 21, 'légère'),
       (264, 40, 'agile'),
       (265, 37, 'sécuritaire'),
       (266, 14, 'spacieuse'),
       (267, 10, 'luxe'),
       (268, 5, 'confortable'),
       (269, 48, 'sobre'),
       (270, 21, 'élégante'),
       (271, 12, 'compacte'),
       (272, 46, 'économique'),
       (273, 39, 'colorée'),
       (274, 6, 'élégante'),
       (275, 15, 'sportive'),
       (276, 4, 'performante'),
       (277, 24, 'luxueuse'),
       (278, 9, 'puissante'),
       (279, 23, 'prestigieuse'),
       (280, 35, 'extravagante'),
       (281, 2, 'puissante'),
       (282, 10, 'légendaire'),
       (283, 42, 'sportive'),
       (284, 27, 'luxe'),
       (285, 9, 'prestigieuse'),
       (286, 15, 'exclusivité'),
       (287, 47, 'ultra-rapide'),
       (288, 37, 'luxe extrême'),
       (289, 42, 'luxe'),
       (290, 50, 'puissance'),
       (291, 21, 'confort'),
       (292, 6, 'grand luxe'),
       (293, 17, 'prestigieuse'),
       (294, 16, 'incomparable');
