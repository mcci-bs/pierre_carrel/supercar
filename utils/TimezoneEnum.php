<?php
/**
 *  enum des timezones
 * */

class TimezoneEnum
{
    const PARIS = 'Europe/Paris';
    const NEW_YORK = 'America/New_York';
    const LONDON = 'Europe/London';
    const PORT_LOUIS = 'Indian/Mauritius';
}

?>
