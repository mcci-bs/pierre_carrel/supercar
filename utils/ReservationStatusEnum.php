<?php
/**
 * enum des les status de reservation
 * */

class ReservationStatusEnum
{
    const EN_COURS = 'En cours';
    const EN_ATTENTE = 'En attente';
    const ANNULER = 'Annuler';
    const EFFECTUER = 'Effectuer';
    const RESERVER = 'Reserver';
}

?>
