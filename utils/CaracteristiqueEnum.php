<?php
// Définir un enum pour regrouper les caractéristiques des voitures
class CaracteristiqueEnum {
const FIABLE = 'Fiable';
const ÉCONOMIQUE = 'Économique';
const SPACIEUSE = 'Spacieuse';
const SPORTIVE = 'Sportive';
const PUISSANTE = 'Puissante';
const LUXE = 'Luxe';
const COMPACTE = 'Compacte';
const ÉCONOME = 'Économe';
const SOBRE = 'Sobre';
const CONFORTABLE = 'Confortable';
const POLYVALENTE = 'Polyvalente';
const IMPRESSIONNANTE = 'Impressionnante';
const ÉLÉGANTE = 'Élégante';
const PERFORMANTE = 'Performante';
const LÉGÈRE = 'Légère';
const AUTONOME = 'Autonome';
const LUXUEUSE = 'Luxueuse';
const SIMPLE = 'Simple';
const BASIQUE = 'Basique';
const PRATIQUE = 'Pratique';
const SÉCURITAIRE = 'Sécuritaire';
const AMUSANTE = 'Amusante';
const COLORÉE = 'Colorée';
const STYLÉE = 'Stylée';
const AGILE = 'Agile';
const INNOVANTE = 'Innovante';
const ROBUSTE = 'Robuste';
const TOUT_TERRAIN = 'Tout-terrain';
const ÉLECTRIQUE = 'Électrique';
const GRAND_LUXE = 'Grand luxe';
const TECHNOLOGIQUE = 'Technologique';
const HAUTES_PERFORMANCES = 'Hautes performances';
const GRAND_TOURISME = 'Grand tourisme';
const LÉGENDAIRE = 'Légendaire';
const ULTRA_RAPIDE = 'Ultra-rapide';
const LUXE_EXTRÊME = 'Luxe extrême';
const EXCLUSIVITÉ = 'Exclusivité';
const PRESTIGIEUSE = 'Prestigieuse';
const INCOMPARABLE = 'Incomparable';
const HAUTE_PERFORMANCE = 'Haute performance';
}
?>