<?php

require_once '../utils/TimezoneEnum.php';

class DateTimeFormatter
{

    /**
     * Formatage de la date
     * @param $timestamp
     * @return false|string
     */
    public static function formatDateTime($timestamp)
    {
        // Utilisation du fuseau horaire par défaut (port louis)
        return self::formatDateTimeWithTimeZone($timestamp, TimezoneEnum::PORT_LOUIS);
    }

    /**
     * Formatage de la date en fonction du fuseau horaire
     * @param $timestamp
     * @param $timezone
     * @return false|string
     */
    public static function formatDateTimeWithTimeZone($timestamp, $timezone)
    {
        // Définition du fuseau horaire
        date_default_timezone_set($timezone);
        // Formatage de la date
        return strftime("%A %e %B %Y %H:%M:%S", $timestamp);
    }

}
