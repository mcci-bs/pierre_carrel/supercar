<?php
require_once '../config/config.php';
/**
 * La classe Database qui gère la connexion à la base de données.
 */
// Database.php
class Database {
    private static $instance;
    private $connection;

    private function __construct($host, $user, $password, $dbname) {
        // Initialiser la connexion à la base de données
        $this->connection = new mysqli($host, $user, $password, $dbname);

        // Vérifier les erreurs de connexion
        if ($this->connection->connect_error) {
            die("Erreur de connexion à la base de données : " . $this->connection->connect_error);
        }
    }

    public static function getInstance() {
        if (!isset(self::$instance)) {
            // informations de la base de données
            self::$instance = new Database(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
        }
        return self::$instance;
    }

    public function getConnection() {
        return $this->connection;
    }

    public function getSecretKey() {
        return JWT_SECRET_KEY;
    }
}


?>
