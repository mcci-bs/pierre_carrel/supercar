<?php

class Car {
    private $id;
    private $marque;
    private $model;
    private $year;
    private $description;
    private $color;
    private $price;
    private $consumption; //la consommation
    private $available; // disponible
    private $caracteristiques; // tableau de caractéristiques
    private $images = []; // Tableau pour stocker les URLs des images



    /**
     * @param $id
     * @param $marque
     * @param $model
     * @param $year
     * @param $description
     * @param $color
     * @param $price
     * @param $consumption
     * @param $available
     * @param $caracteristiques
     * @param $images
     *
     */
    public function __construct($id, $marque, $model, $year, $description, $color, $price, $consumption,$available, $caracteristiques = [], $images = [])
    {
        $this->id = $id;
        $this->marque = $marque;
        $this->model = $model;
        $this->year = $year;
        $this->description = $description;
        $this->color = $color;
        $this->price = $price;
        $this->consumption = $consumption;
        $this->caracteristiques = $caracteristiques;
        $this->images = $images;
        $this->available = $available;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getMarque()
    {
        return $this->marque;
    }

    public function getModel()
    {
        return $this->model;
    }

    public function getYear()
    {
        return $this->year;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getColor()
    {
        return $this->color;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getConsumption()
    {
        return $this->consumption;
    }

    public function getCaracteristiques()
    {
        return $this->caracteristiques;
    }

    // Méthode pour récupérer toutes les images de la voiture
    public function getImages()
    {
        return $this->images;
    }

    // Méthode pour ajouter une image à la voiture
    public function addImage($url) {
        $this->images[] = $url;
    }


    public function getAvailable()
    {
        return $this->available;
    }


}

?>
