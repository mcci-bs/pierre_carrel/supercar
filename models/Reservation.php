<?php
class Reservation {
    private $id;
    private $userId;
    private $car_id;
    private $date;
    private $startTime;
    private $endTime;
    private $status; // Status : encour, effectuer, annuler, en attente

    /**
     * @param $id
     * @param $userId
     * @param $car_id
     * @param $date
     * @param $startTime
     * @param $endTime
     * @param $status
     */
    public function __construct($id,$userId, $car_id, $date, $startTime, $endTime, $status)
    {
        $this->id = $id;
        $this->userId = $userId;
        $this->car_id = $car_id;
        $this->date = $date;
        $this->startTime = $startTime;
        $this->endTime = $endTime;
        $this->status = $status;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function getCarId()
    {
        return $this->car_id;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function getStartTime()
    {
        return $this->startTime;
    }

    public function getEndTime()
    {
        return $this->endTime;
    }

    public function getStatus()
    {
        return $this->status;
    }



}

?>
