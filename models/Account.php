<?php
class Account {
    private $id;
    private $userId;
    private $username;
    private $hashedPassword; // Stocker le mot de passe haché
    private $role;

    /**
     * @param $id
     * @param $userId
     * @param $username
     * @param $hashedPassword
     * @param $role
     */
    public function __construct($id, $userId, $username, $hashedPassword, $role)
    {
        $this->id = $id;
        $this->userId = $userId;
        $this->username = $username;
        $this->hashedPassword = $hashedPassword;
        $this->role = $role;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getPassword()
    {
        return $this->hashedPassword;
    }

    public function getRole()
    {
        return $this->role;
    }


}

?>
