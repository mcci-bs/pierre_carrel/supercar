<?php

/**
 * La classe JWTGenerator qui est utilisée pour générer les tokens JWT.
 */
class JWTGenerator
{
    private static $secretKey = "votre_clé_secrète";
    private static $algorithm = 'HS256'; // Algorithme de chiffrement

    public static function generateToken($payload)
    {
        $header = json_encode(['typ' => 'JWT', 'alg' => self::$algorithm]);
        $payload = json_encode($payload);

        $base64UrlHeader = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($header));
        $base64UrlPayload = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($payload));

        $signature = hash_hmac(self::$algorithm, $base64UrlHeader . "." . $base64UrlPayload, self::$secretKey, true);
        $base64UrlSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature));

        return $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;
    }

    public static function decodeToken($token)
    {
        list($headerEncoded, $payloadEncoded, $signatureEncoded) = explode('.', $token);
        $payload = json_decode(base64_decode(str_replace(['-', '_'], ['+', '/'], $payloadEncoded)), true);
        return $payload;
    }
}

?>
