<?php
class User {
    private $id;
    private $firstName;
    private $lastName;
    private $phone;
    private $email;
    // Ajoutez d'autres attributs publics et privés selon vos besoins

    public function __construct($id, $firstName, $lastName,$email,$phone) {
        $this->id = $id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->phone = $phone;
        $this->email = $email;
        // Initialisez d'autres attributs publics et privés ici
    }

    public function getId() {
        return $this->id;
    }

    public function getFirstName() {
        return $this->firstName;
    }

    public function getLastName() {
        return $this->lastName;
    }

    public function getPhone() {
        return $this->phone;
    }

    public function getEmail() {
        return $this->email;
    }
}

?>
