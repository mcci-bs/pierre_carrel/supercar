<?php
// Inclusion du modèle de réservation
require_once '../models/Reservation.php';
require_once '../core/Database.php';
require_once '../repositories/ReservationRepository.php';

class ReservationController
{
    // Méthode pour effectuer une réservation
    public static function makeReservation($userId, $carId, $reservationDate, $startTime, $endTime)
    {
        ReservationRepository::init(Database::getInstance());
        // Vérifier si la reservation existe déjà
        if (ReservationRepository::checkExistingReservation($carId, $reservationDate, $startTime, $endTime)) {
            return false; // La reservation existe déjà, retourner false
        }

        // Enregistrer l'utilisateur
        $reservation = ReservationRepository::makeReservation($userId, $carId, $reservationDate, $startTime, $endTime);

        // Gestion du succès ou de l'échec de la réservation
        if ($reservation) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Recuperer la liste des reservations en fontion du role de l'utilisateur admin ou client et de l'id de l'utilisateur
     * @param $userId:id de l'utilisateur
     * @param $userRole: role de l'utilisateur
     * @return array : tableau de reservation ou un tableau vide
     */
    public static function getReservations($userId, $userRole)
    {
        ReservationRepository::init(Database::getInstance()); // Initialise la connexion à la base de données
        $reservations = ReservationRepository::getReservations($userId, $userRole);
        if ($reservations) {
            return $reservations;
        } else {
            return [];
        }
    }
}

?>
