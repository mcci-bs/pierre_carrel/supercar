<?php
require_once '../repositories/UserRepository.php';
require_once '../models/User.php';
require_once '../models/JWTGenerator.php';
require_once '../core/Database.php';
require_once '../utils/DateTimeFormatter.php';

class AuthController {
    private static $secretKey = JWT_SECRET_KEY;

    public static function authenticate($username, $password) {
        UserRepository::init(Database::getInstance()); // Initialise la connexion à la base de données
        // Vérifier les informations d'identification en utilisant le repository des utilisateurs
        $account = UserRepository::getUserByUsername($username);
        if ($account && self::verifyPassword($account, $password)) {
            // Générer un token JWT
            $token = self::generateToken($account);
            return $token;
        } else {
            return false;
        }
    }

    private static function generateToken($account) {
        $expirationTime = time() + (60 * 60); // 1 heure à partir de maintenant
        $expirationDateTime = DateTimeFormatter::formatDateTime($expirationTime); // Format de date et heure lisible
        $data = UserRepository::getUserByUsernameInformation($account->getUsername());
        $payload = [
            'id' => $data->getId(),
            'firstName' => $data->getFirstName(),
            'lastName' => $data->getLastName(),
            'email' => $data->getEmail(),
            'phone' => $data->getPhone(),
            'role' => $account->getRole(),
            'exp' => $expirationDateTime // Date et heure d'expiration lisible
        ];
        return JWTGenerator::generateToken($payload);
    }
    // Méthode pour invalider le token de l'utilisateur
    public static function invalidateToken() {
        unset($_SESSION['token']);
    }

    public static function registerUser($username, $password, $firstName, $lastName,$email,$phone) {
        UserRepository::init(Database::getInstance()); // Initialise la connexion à la base de données
        // Vérifier si l'utilisateur existe déjà
        if (UserRepository::getUserByUsername($username)) {
            return false; // L'utilisateur existe déjà, retourner false
        }

        // Enregistrer l'utilisateur
        $user = UserRepository::registerUser($username, $password, $firstName, $lastName, $email,$phone);

        if ($user) {
            // L'utilisateur a été enregistré avec succès, vous pouvez le connecter automatiquement ici si nécessaire
            return true;
        } else {
            return false; // Échec de l'enregistrement de l'utilisateur
        }
    }

    public static function getCurrentUser() {
        // Récupérer l'utilisateur actuellement connecté à partir du token JWT dans l'en-tête Authorization
        if (isset($_SERVER['HTTP_AUTHORIZATION']) && preg_match('/Bearer\s(\S+)/', $_SERVER['HTTP_AUTHORIZATION'], $matches)) {
            $token = $matches[1];
            try {
                $decoded = JWT::decode($token, self::$secretKey, array('HS256'));
                // Vous pouvez maintenant utiliser les informations de l'utilisateur dans $decoded
                return $decoded;
            } catch (Exception $e) {
                // La validation du token a échoué
                return null;
            }
        } else {
            return null;
        }
    }

    // Méthode pour vérifier si le mot de passe correspond
    private static function verifyPassword($account, $password) {
        return password_verify($password, $account->getPassword());
    }

    public static function getUserInfoFromToken($token) {
        try {
            // Décoder le token JWT en utilisant la clé secrète
            $decoded = JWTGenerator::decodeToken($token);
            return $decoded;
        } catch (Exception $e) {
            // La validation du token a échoué
            return null;
        }
    }
}
?>
