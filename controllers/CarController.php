<?php
require_once '../repositories/CarRepository.php';
require_once '../core/Database.php';
require_once '../models/Car.php';
require_once '../utils/CaracteristiqueEnum.php';

class CarController
{

    public static function getCarsWithPicture($userRole)
    {
        CarRepository::init(Database::getInstance()); // Initialise la connexion à la base de données
        // Recuperer la liste des voiture en fontion du role de l'utilisateur admin ou client
        $cars = CarRepository::getCarsWithPicture($userRole);
        if ($cars) {
            return $cars;
        } else {
            return [];
        }
    }
    public static function getCars($userRole)
    {
        CarRepository::init(Database::getInstance()); // Initialise la connexion à la base de données
        // Recuperer la liste des voiture en fontion du role de l'utilisateur admin ou client
        $cars = CarRepository::getCars($userRole);
        if ($cars) {
            return $cars;
        } else {
            return [];
        }
    }

    public static function getCarDetails($car_id)
    {
        CarRepository::init(Database::getInstance()); // Initialise la connexion à la base de données
        $car_details = CarRepository::getCarDetails($car_id);
        return $car_details;
    }


    /**
     * Methode permetant de mettre a jour les information d'une voirure
     * @param $car_id :identifiant de la voiture
     * @param $marque :marque
     * @param $model :le model
     * @param $year :l'annee de mise en circulation
     * @param $description :description sur la voiture
     * @param $color :la couleur de la voiture
     * @param $price :le prix de la voiture
     * @param $consumption :consommation de la voiture
     * @param $caracteristiques :liste des caracteristique de la voitures
     * @param $images : tableau des images representant la voiture
     * @param $oldImages : tableau des anciennes immages representant la voitures
     * @return bool :valeur de retour si tous le process c'est bien derouler peux etre true ou false
     */
    public static function updateCarAndImages($car_id, $marque, $model, $year, $description, $color, $price, $consumption, $caracteristiques, $images, $oldImages)
    {// Vérifier si des images ont été téléchargées
        $images = self::checkUploadedImages($images);
        CarRepository::init(Database::getInstance()); // Initialise la connexion à la base de données
        // Continuer seulement si des images ont été téléchargées
        if (!empty($images)) {
            $folder = "../uploads/"; //repertoire ou sera stocker l'image
            // Mettre à jour les informations de la voiture
            $success = CarRepository::updateCar($car_id, $marque, $model, $year, $description, $color, $price, $consumption, $caracteristiques);
            // Vérifier si la mise à jour des informations de la voiture a réussi
            $imagesUrls = [];
            if ($success) {
                CarController::deleteImageFromURL($oldImages, $folder);
                // Télécharger et enregistrer les images
                foreach ($images['tmp_name'] as $key => $tmp_name) {
                    $image = $images['name'][$key];
                    $temp = $images['tmp_name'][$key];
                    // Extraire l'extension du nom de fichier
                    $extension = pathinfo($image, PATHINFO_EXTENSION);
                    // Générer un identifiant unique
                    $uniqueId = uniqid();
                    // Construire le nouveau nom de fichier avec l'identifiant unique et l'extension d'origine
                    $new_image_name = $uniqueId . '.' . $extension;
                    if (move_uploaded_file($temp, $folder . $new_image_name)) {
                        $imagesUrls[] = $folder . $new_image_name;  // ajouter le chemin de l'image dans le tableau a image url
                    }
                }
            }
            // Insérer le chemin de l'image dans la base de données
            CarRepository::insertImage($car_id, $imagesUrls);
            return $success;
        } else {
            // Aucune image n'a été téléchargée, donc il n'y a pas de mise à jour des images
            return CarRepository::updateCar($car_id, $marque, $model, $year, $description, $color, $price, $consumption, $caracteristiques);
        }


    }


    /**
     * Mettre à jour la disponibilité de la voiture avec la nouvelle valeur
     * @param $car_id
     * @param $available
     * @return void
     */
    public static function toggleAvailability($car_id, $available) {
        CarRepository::init(Database::getInstance()); // Initialise la connexion à la base de données
        CarRepository::updateAvailability($car_id, $available);
    }

    /**
     * Affiche toutes les caractéristiques contenue dans
     * CaracteristiqueEnum
     * @return array
     */
    public static function getCaracteristiquesList()
    {
        $reflectionClass = new ReflectionClass('CaracteristiqueEnum');
        $constants = $reflectionClass->getConstants();
        return array_values($constants);
    }

    /**
     * Vérifier si des images ont été téléchargées
     * @param $images : tableau des images representant la voiture
     * @return array : retourne un tableau vide ou contenant les images
     */
    private static function checkUploadedImages($images)
    {
        if (isset($images['name'])) {
            $validImages = [];

            // Vérifier si au moins un fichier image a été sélectionné
            foreach ($images['name'] as $key => $name) {
                if (!empty($name)) {
                    // S'il y a un nom de fichier, il y a une image valide
                    $validImages['name'][] = $images['name'][$key];
                    $validImages['type'][] = $images['type'][$key];
                    $validImages['tmp_name'][] = $images['tmp_name'][$key];
                    $validImages['error'][] = $images['error'][$key];
                    $validImages['size'][] = $images['size'][$key];
                }
            }

            return $validImages;
        } else {
            // Aucun fichier image sélectionné, retourner un tableau vide
            return [];
        }
    }



    /**
     * Supprimer les images inutiliser dans le repertoire upload
     * @param $urls
     * @param $directory
     * @return void
     */
    public static function deleteImageFromURL($urls, $directory)
    {

        foreach ($urls as $url) {
            // Extraire le nom de fichier de l'URL
            $fileName = basename($url);
            // Chemin complet du fichier à supprimer
            $filePath = $directory . $fileName;
            // Vérifier si le fichier existe avant de le supprimer
            if (file_exists($filePath)) {
                // Supprimer le fichier
                unlink($filePath);
            }
        }
    }


}

?>
