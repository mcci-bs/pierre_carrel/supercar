<?php
/**
 * UserRepository.php, contient les requêtes SQL liées aux utilisateurs.
 */
require_once '../models/Car.php';

class CarRepository
{
    private static $db;

    public static function init($database)
    {
        self::$db = $database;
    }


    /**
     * Liste des voitures en fonction des role d'un utilisateur
     * @param $userRole: role de l'utilisateur (client ou admin))
     * @return array
     */
    public static function getCars($userRole)
    {
        if($userRole=='client'){
            $query = "SELECT v.* FROM voiture v where v.disponible = true";
        }else{
            $query = "SELECT v.* FROM voiture v";
        }

        $stmt = self::$db->getConnection()->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $cars = [];
        while ($row = $result->fetch_assoc()) {
            $cars[] = new Car($row['id'], $row['marque'], $row['modele'], $row['annee'], $row['description'], $row['couleur'], $row['prix'], $row['consommation'], $row['disponible']);
        }
        return $cars;
    }

    /**
     * Liste des voitures avec images en fonction des role d'un utilisateur
     * @param $userRole: role de l'utilisateur (client ou admin))
     * @return array
     */
    public static function getCarsWithPicture($userRole)
    {
        if($userRole == 'client') {
            $query = "SELECT v.*, i.url AS image_url FROM voiture v 
                  LEFT JOIN image i ON v.id = i.voiture_id 
                  WHERE v.disponible = true";
        } else {
            $query = "SELECT v.*, i.url AS image_url FROM voiture v 
                  LEFT JOIN image i ON v.id = i.voiture_id";
        }

        $stmt = self::$db->getConnection()->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $cars = [];

        while ($row = $result->fetch_assoc()) {
            // Si la voiture n'a pas encore été ajoutée à la liste, on l'ajoute
            if (!isset($cars[$row['id']])) {
                $car = new Car($row['id'], $row['marque'], $row['modele'], $row['annee'], $row['description'], $row['couleur'], $row['prix'], $row['consommation'], $row['disponible']);
                $cars[$row['id']] = $car;
            }

            // Ajouter l'image à la voiture
            if (!empty($row['image_url'])) {
                $cars[$row['id']]->addImage($row['image_url']);
            }
        }

        return array_values($cars); // Convertir l'array associatif en array indexé pour avoir une liste ordonnée des voitures
    }



    /**
     * Mettre a jour la disponibilite de la voiture
     * @param $car_id
     * @param $available
     * @return mixed
     */
    public static function updateAvailability($car_id, $available) {
        // Inverser la valeur de disponibilité (si on a true au depart alors inverser en false)
        $newAvailability = !$available;

        // Préparer la requête SQL
        $query = "UPDATE voiture SET disponible = ? WHERE id = ?";

        // Exécuter la requête avec les paramètres
        $stmt = self::$db->getConnection()->prepare($query);
        $stmt->bind_param("ii", $newAvailability, $car_id);

        // Exécuter la requête et retourner le résultat
        return $stmt->execute();
    }

    public static function getCarDetails($car_id)
    {
        $query = "SELECT v.*, 
                    GROUP_CONCAT(DISTINCT c.libelle SEPARATOR ', ') AS caracteristiques, 
                    GROUP_CONCAT(DISTINCT i.url SEPARATOR ', ') AS images
                    FROM voiture v
                    LEFT JOIN caracteristique c ON v.id = c.voiture_id
                    LEFT JOIN image i ON v.id = i.voiture_id
                    WHERE v.id = ?
                    GROUP BY v.id, v.marque, v.modele, v.annee, v.description, v.couleur, v.prix, v.consommation";

        $stmt = self::$db->getConnection()->prepare($query);
        $stmt->bind_param("i", $car_id);
        $stmt->execute();
        $result = $stmt->get_result();

        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            return new Car(
                $row['id'],
                $row['marque'],
                $row['modele'],
                $row['annee'],
                $row['description'],
                $row['couleur'],
                $row['prix'],
                $row['consommation'],
                $row['disponible'],
                explode(', ', $row['caracteristiques']),
                explode(', ', $row['images'])
            );
        } else {
            return null;
        }
    }

    public static function updateCar($car_id, $marque, $model, $year, $description, $color, $price, $consumption, $caracteristiques)
    {
        // Commencer la transaction
        self::$db->getConnection()->begin_transaction();

        try {
            // Mettre à jour les détails de base de la voiture
            $query = "UPDATE voiture SET marque=?, modele=?, annee=?, description=?, couleur=?, prix=?, consommation=? WHERE id=?";
            $stmt = self::$db->getConnection()->prepare($query);
            $stmt->bind_param("ssissssi", $marque, $model, $year, $description, $color, $price, $consumption, $car_id);
            $stmt->execute();
            $stmt->close();

            // Supprimer les caractéristiques existantes de la voiture
            $deleteQuery = "DELETE FROM caracteristique WHERE voiture_id=?";
            $deleteStmt = self::$db->getConnection()->prepare($deleteQuery);
            $deleteStmt->bind_param("i", $car_id);
            $deleteStmt->execute();
            $deleteStmt->close();

            // Insérer les nouvelles caractéristiques
            $insertCaracteristiqueQuery = "INSERT INTO caracteristique (voiture_id, libelle) VALUES (?, ?)";
            $insertCaracteristiqueStmt = self::$db->getConnection()->prepare($insertCaracteristiqueQuery);
            foreach ($caracteristiques as $caracteristique) {
                $insertCaracteristiqueStmt->bind_param("is", $car_id, $caracteristique);
                $insertCaracteristiqueStmt->execute();
            }
            $insertCaracteristiqueStmt->close();

            // Valider la transaction
            self::$db->getConnection()->commit();

            return true; // Tout s'est bien passé, retourner true
        } catch (Exception $e) {
            // En cas d'erreur, annuler la transaction et enregistrer l'erreur
            self::$db->getConnection()->rollback();
            error_log("Erreur lors de la mise à jour de la voiture : " . $e->getMessage());
            return false;
        }
    }


    /**
     * Insertion de l'url d'une image de voiture dans la bd
     * @param $car_id
     * @param $image_url
     * @return bool
     */
    public static function insertImage($car_id, $images)
    {
        self::$db->getConnection()->begin_transaction();
        try {
            // Supprimer les images existantes de la voiture
            $deleteImageQuery = "DELETE FROM image WHERE voiture_id=?";
            $deleteImageStmt = self::$db->getConnection()->prepare($deleteImageQuery);
            $deleteImageStmt->bind_param("i", $car_id);
            $deleteImageStmt->execute();
            $deleteImageStmt->close();

            // Insérer les nouvelles images
            $insertImageQuery = "INSERT INTO image (voiture_id, url) VALUES (?, ?)";
            $insertImageStmt = self::$db->getConnection()->prepare($insertImageQuery);
            foreach ($images as $image) {
                $insertImageStmt->bind_param("is", $car_id, $image);
                $insertImageStmt->execute();
            }
            $insertImageStmt->close();
            // Valider la transaction
            self::$db->getConnection()->commit();
            return true; // Tout s'est bien passé, retourner true
        } catch (Exception $e) {
            // En cas d'erreur, annuler la transaction et enregistrer l'erreur
            self::$db->getConnection()->rollback();
            error_log("Erreur lors de la mise à jour de la voiture : " . $e->getMessage());
            return false;
        }


    }



    public static function getCar($id)
    {
        $query = "SELECT * FROM voiture WHERE id = ?";
        $stmt = self::$db->getConnection()->prepare($query);
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $result = $stmt->get_result();

        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            // Retourner un objet car
            return new Car($row['id'], $row['marque'], $row['modele'], $row['annee'], $row['description'], $row['couleur'], $row['prix'], $row['consommation'], $row['disponible']);
        } else {
            return null;
        }
    }

}

?>
