<?php
/**
 * UserRepository.php, contient les requêtes SQL liées aux utilisateurs.
 */
require_once '../models/Reservation.php';
require_once '../utils/ReservationStatusEnum.php';

class ReservationRepository
{
    private static $db;

    public static function init($database)
    {
        self::$db = $database;
    }

    public static function checkExistingReservation($carId, $reservationDate, $startTime, $endTime) {
        // Convertir les dates et heures en format compatible avec la base de données
        $reservationDate = date('Y-m-d', strtotime($reservationDate));
        $startTime = date('H:i:s', strtotime($startTime));
        $endTime = date('H:i:s', strtotime($endTime));

        // Préparer la requête SQL
        $query = "SELECT COUNT(*) AS count FROM reservation WHERE voiture_id = ? AND date_reservation = ? AND ((heure_debut_reservation >= ? AND heure_debut_reservation < ?) OR (heure_fin_reservation > ? AND heure_fin_reservation <= ?))";
        $stmt = self::$db->getConnection()->prepare($query);

        // Binder les valeurs aux paramètres de la requête
        $stmt->bind_param("ssssss", $carId, $reservationDate, $startTime, $endTime, $startTime, $endTime);

        // Exécuter la requête
        $stmt->execute();

        // Récupérer le résultat
        $result = $stmt->get_result();
        $row = $result->fetch_assoc();

        // Retourner vrai si une réservation existe déjà pour la même voiture, la même date et la même heure de début ou de fin
        return $row['count'] > 0;
    }

    public static function makeReservation($userId, $carId, $reservationDate, $startTime, $endTime) {
        // Statut de la reservation
        $reservationStatus = ReservationStatusEnum::EN_ATTENTE;

        // Convertir les dates et heures en format compatible avec la base de données
        $reservationDate = date('Y-m-d', strtotime($reservationDate));
        $startTime = date('H:i:s', strtotime($startTime));
        $endTime = date('H:i:s', strtotime($endTime));

        // Préparer la requête SQL
        $query = "INSERT INTO reservation (utilisateur_id, voiture_id, date_reservation, heure_debut_reservation, heure_fin_reservation, statut) VALUES (?, ?, ?, ?, ?, ?)";
        $stmt = self::$db->getConnection()->prepare($query);

        // Vérifier si la préparation de la requête a échoué
        if (!$stmt) {
            // Gérer l'erreur de préparation de la requête
            error_log("Erreur lors de la préparation de la requête : " . self::$db->getConnection()->error);
            return false;
        }

        // Binder les valeurs aux paramètres de la requête
        $stmt->bind_param("ssssss", $userId, $carId, $reservationDate, $startTime, $endTime, $reservationStatus);

        // Exécuter la requête
        $success = $stmt->execute();
        // Vérifier si l'exécution de la requête a échoué
        if (!$success) {
            // Gérer l'erreur d'exécution de la requête
            error_log("Erreur lors de l'exécution de la requête : " . $stmt->error);
            return false;
        }

        // Retourner true si la réservation a été ajoutée avec succès
        return $success;
    }

    public static function getReservations($userId, $userRole)
    {
        if ($userRole == 'client') {
            $query = "SELECT r.*, v.marque, v.modele, u.nom, u.prenom, i.url AS image_url
                  FROM reservation r 
                  INNER JOIN voiture v ON r.voiture_id = v.id 
                  LEFT JOIN image i ON v.id = i.voiture_id
                  INNER JOIN utilisateur u ON r.utilisateur_id = u.id 
                  WHERE r.utilisateur_id = ?";
            // Préparer la requête SQL
            $stmt = self::$db->getConnection()->prepare($query);
            // Binder les valeurs aux paramètres de la requête
            $stmt->bind_param("i", $userId);
        } else {
            $query = "SELECT r.*, v.marque, v.modele, u.nom, u.prenom, i.url AS image_url
                  FROM reservation r 
                  INNER JOIN voiture v ON r.voiture_id = v.id 
                  LEFT JOIN image i ON v.id = i.voiture_id
                  INNER JOIN utilisateur u ON r.utilisateur_id = u.id";
            // Préparer la requête SQL
            $stmt = self::$db->getConnection()->prepare($query);
        }

        // Exécuter la requête
        $stmt->execute();
        $result = $stmt->get_result();
        $reservations = [];

        while ($row = $result->fetch_assoc()) {
            // Vérifiez si la réservation existe déjà dans le tableau
            $reservationId = $row['id'];
            $existingReservation = isset($reservations[$reservationId]) ? $reservations[$reservationId] : null;

            // Si la réservation n'existe pas encore, créez une nouvelle entrée dans le tableau
            if (!$existingReservation) {
                $reservation = [
                    'id' => $row['id'],
                    'utilisateur_id' => $row['utilisateur_id'],
                    'voiture_id' => $row['voiture_id'],
                    'date_reservation' => $row['date_reservation'],
                    'heure_debut_reservation' => $row['heure_debut_reservation'],
                    'heure_fin_reservation' => $row['heure_fin_reservation'],
                    'statut' => $row['statut'],
                    'marque' => $row['marque'],
                    'modele' => $row['modele'],
                    'nom_utilisateur' => $row['nom'],
                    'prenom_utilisateur' => $row['prenom'],
                    'image_urls' => [], // Initialisez un tableau pour stocker les URL des images
                ];
            } else {
                // Sinon, utilisez la réservation existante
                $reservation = $existingReservation;
            }

            // Ajoutez l'URL de l'image à la liste des images de la voiture
            if ($row['image_url']) {
                $reservation['image_urls'][] = $row['image_url'];
            }

            // Mettez à jour ou ajoutez la réservation dans le tableau final
            $reservations[$reservationId] = $reservation;
        }

        return $reservations;
    }



    public static function getReservation($reservationId) {
        $query = "SELECT r.*, v.marque, v.modele, u.nom, u.prenom 
              FROM reservation r 
              INNER JOIN voiture v ON r.voiture_id = v.id 
              INNER JOIN utilisateur u ON r.utilisateur_id = u.id 
              WHERE r.id = ?";
        // Préparer la requête SQL
        $stmt = self::$db->getConnection()->prepare($query);
        // Binder les valeurs aux paramètres de la requête
        $stmt->bind_param("i", $reservationId);

        // Exécuter la requête
        $stmt->execute();
        $result = $stmt->get_result();

        // Vérifier s'il y a une réservation correspondante
        if ($result->num_rows === 0) {
            return null; // Aucune réservation trouvée
        } else {
            // Récupérer les données de la réservation
            $row = $result->fetch_assoc();
            // Créer un tableau associatif pour la réservation
            $reservation = [
                'id' => $row['id'],
                'utilisateur_id' => $row['utilisateur_id'],
                'voiture_id' => $row['voiture_id'],
                'date_reservation' => $row['date_reservation'],
                'heure_debut_reservation' => $row['heure_debut_reservation'],
                'heure_fin_reservation' => $row['heure_fin_reservation'],
                'statut' => $row['statut'],
                'marque' => $row['marque'],
                'modele' => $row['modele'],
                'nom_utilisateur' => $row['nom'],
                'prenom_utilisateur' => $row['prenom']
            ];
            return $reservation;
        }
    }



}

?>
