<?php
/**
 * UserRepository.php, contient les requêtes SQL liées aux utilisateurs.
 */
require_once '../models/User.php';
require_once '../models/Account.php';

class UserRepository
{
    private static $db;

    public static function init($database)
    {
        self::$db = $database;
    }

    public static function registerUser($username, $password, $firstName, $lastName, $email, $phone)
    {

        // Hasher le mot de passe pour insertion dans la table utilisateur
        $hashedPassword = password_hash($password, PASSWORD_DEFAULT);
        // Préparer la requête SQL
        $query = "INSERT INTO utilisateur (nom, prenom, email, telephone) VALUES (?, ?, ?, ?)";
        $stmt = self::$db->getConnection()->prepare($query);
        // Binder les valeurs aux paramètres de la requête
        $stmt->bind_param("ssss", $lastName, $firstName, $email, $phone);
        // Exécuter la requête
        $stmt->execute();
        // Récupérer l'ID de l'utilisateur nouvellement inséré
        $userId = self::$db->getConnection()->insert_id;

        $userRole = self::userRole($phone);

        // Préparer la requête SQL pour insertion dans la table compte
        $query = "INSERT INTO compte (username, password, user_id, role) VALUES (?, ?, ?,?)";
        $stmt = self::$db->getConnection()->prepare($query);
        // Binder les valeurs aux paramètres de la requête
        $stmt->bind_param("ssss", $username, $hashedPassword, $userId, $userRole);
        // Exécuter la requête
        $stmt->execute();

        // Créer et retourner une instance de la classe User
        return new User($userId, $firstName, $lastName, $email, $phone);
    }

    // Méthode pour récupérer un utilisateur par son nom d'utilisateur
    public static function getUserByUsername($username)
    {
        $query = "SELECT * FROM compte WHERE username = ?";
        $stmt = self::$db->getConnection()->prepare($query);
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $result = $stmt->get_result();
        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            return new Account($row['id'], $row['user_id'], $row['username'], $row['password'], $row['role']);
        } else {
            return null;
        }
    }

    public static function getUserByUsernameInformation($username)
    {
        $query = "SELECT * FROM compte WHERE username = ?";
        $stmt = self::$db->getConnection()->prepare($query);
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $result = $stmt->get_result();
        $row = $result->fetch_assoc();
        $userId = $row['user_id'];

        // Utiliser l'ID récupéré pour récupérer l'utilisateur associé
        $queryUser = "SELECT * FROM utilisateur WHERE id = ?";
        $stmtUser = self::$db->getConnection()->prepare($queryUser);
        $stmtUser->bind_param("i", $userId);
        $stmtUser->execute();
        $resultUser = $stmtUser->get_result();

        if ($resultUser->num_rows > 0) {
            $rowUser = $resultUser->fetch_assoc();
            // Retourner un objet user
            return new User($rowUser['id'], $rowUser['prenom'], $rowUser['nom'], $rowUser['email'], $rowUser['telephone']);
        } else {
            return null;
        }
    }


    /**
     * Recupere un utilisateur en fonction de son id
     * @param $id :identifiant de l'utilisateur
     * @return User|null: retourne un objet user  s'il existe en bd  autrement retourne null
     */
    public static function getUser($id)
    {
        $query = "SELECT * FROM utilisateur WHERE id = ?";
        $stmt = self::$db->getConnection()->prepare($query);
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $result = $stmt->get_result();

        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            // Retourner un objet user
            return new User($row['id'], $row['prenom'], $row['nom'], $row['email'], $row['telephone']);
        } else {
            return null;
        }
    }


    /**
     * retourne le type d'utilisateur
     * @param $phone
     * @return string
     */
    private static function userRole($phone)
    {
        if ($phone == "58311155" || $phone == "000000") {
            return "admin";
        } else {
            return "client";
        }
    }
}

?>
