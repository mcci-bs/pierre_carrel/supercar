<?php
// Vérifier si l'ID de la voiture est spécifié dans l'URL
if (!isset($_GET['voiture_id'])) {
    // Rediriger l'utilisateur vers une page d'erreur
    header("Location: notfound.php");
    exit(); // sortir du script après la redirection
}

?>
