<?php
session_start();
require_once '../controllers/AuthController.php';
require_once '../controllers/CarController.php';
require_once '../controllers/ReservationController.php';
require_once '../utils/ReservationStatusEnum.php';


// Vérifier si l'utilisateur est connecté
if (!isset($_SESSION['token'])) {
    // Rediriger l'utilisateur vers la page de connexion s'il n'est pas connecté
    header("Location: login.php");
    exit();
}

// Extraire les informations de l'utilisateur à partir du token
$token = $_SESSION['token'];
$userInfo = AuthController::getUserInfoFromToken($token);
$userId = $userInfo['id'];
$userRole = $userInfo['role'];


$cars00 = CarController::getCars($userRole);
$cars = CarController::getCarsWithPicture($userRole);
$reservations = ReservationController::getReservations($userId, $userRole);

// informations de l'utilisateur et les informations sur les voitures dans un tableau associatif
$data = [
    'userInfo' => $userInfo,
    'cars' => $cars
];

// Vérifier si le formulaire a été soumis
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['toggle_availability'])) {
    // Récupérer les données du formulaire
    $car_id = $_POST['car_id'];
    $available = $_POST['available'];

    // Appeler la méthode pour activer ou désactiver en fonction de la valeur actuelle de available
    CarController::toggleAvailability($car_id, $available);

    // Rediriger vers la même page pour éviter les re-soumissions de formulaire
    header("Location: " . $_SERVER['PHP_SELF']);
    exit();
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php
        $title = ($userRole == 'admin') ? 'Les réservations :' : 'Mes réservations :';
        echo $title;
        ?></title>
    <link href="../resources/css/application.css" rel="stylesheet" type="text/css" media="all"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href='//fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
    <!--slider-->
    <script src="../resources/js/jquery.min.js"></script>
    <script src="../resources/js/script.js" type="text/javascript"></script>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            border: 1px solid black;
            padding: 8px;
            text-align: left;
        }

        tr:nth-child(even) {
            background-color: #f2f2f2;
        }
    </style>
</head>
<body>
<!--Debut Vue client-->
<div class="header-bg">
    <div class="wrap">
        <div class="h-bg">
            <div class="total">
                <div class="header">
                    <div class="box_header_user_menu">
                        <ul class="user_menu">
                            <li class="act first">Bienvenue <?php echo $data['userInfo']['lastName']; ?> </li>
                            <?php if (!$userId) : ?>
                                <li class=""><a href="register.php">
                                        <div class="button-t"><span>S'inscrire</span></div>
                                    </a></li>
                                <li class="last"><a href="login.php">
                                        <div class="button-t"><span>Log in</span></div>
                                    </a></li>
                            <?php else : ?>
                                <li class="last"><a href="logout.php">
                                        <div class="button-t"><span>Se déconnecter</span></div>
                                    </a></li>
                            <?php endif; ?>
                        </ul>
                    </div>

                    <div class="header-bot">
                        <div class="logo">
                            <a href="index.html"><img src="../resources/images/logo.png" alt=""/></a>
                        </div>
                        <div class="search">
                            <input type="text" class="textbox" value="" onfocus="this.value = '';"
                                   onblur="if (this.value == '') {this.value = '';}">
                            <button class="gray-button"><span>Search</span></button>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="menu">
                    <div class="top-nav">
                        <ul>
                            <li><a href="home.php">Home</a></li>
                            <li><a href="#">About</a></li>
                            <li><a href="cars.php">Voitures</a></li>
                            <li class="active"><a href="show_reservation.php">
                                    <?php $title = ($userRole == 'admin') ? 'Les réservations' : 'Réservations';
                                    echo $title; ?></a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="banner-top">
                    <div class="header-bottom">
                        <div class="header_bottom_right_images">
                            <div class="content-wrapper">
                                <div class="content-top">
                                    <div class="box_wrapper"><h1><?php
                                            $title = ($userRole == 'admin') ? 'Les réservations' : 'Mes réservations';
                                            echo $title;
                                            ?></h1>
                                    </div>
                                    <div class="text">
                                        <?php foreach ($reservations as $reservation) : ?>

                                            <?php $images = $reservation['image_urls']; ?>
                                            <?php if (!empty($images)) : ?>
                                                <?php $randomIndex = array_rand($images); ?>
                                                <?php $randomImage = $images[$randomIndex]; ?>
                                            <?php endif; ?>
                                                <div class="grid_1_of_3 images_1_of_3">
                                                    <div class="grid_1">
                                                        <a href="details.php?car_id=<?php echo $reservation['voiture_id']; ?>">
                                                            <?php if (!empty($images)) : ?>
                                                                <img src="<?php echo $randomImage; ?>"
                                                                     title="continue reading" alt="">
                                                            <?php else : ?>
                                                                <img src="../resources/images/banner4.jpg"
                                                                     title="continue reading" alt="">
                                                            <?php endif; ?>
                                                        </a>
                                                        <div class="grid_desc">
                                                            <p class="title"><?php echo $reservation['marque'] . ' ' . $reservation['modele']; ?></p>
                                                            <?php if ($userRole == 'admin') : ?>
                                                                <p class="title1">
                                                                    Utilisateur: <?php echo $reservation['nom_utilisateur'] . ' ' . $reservation['prenom_utilisateur']; ?></p>
                                                            <?php endif; ?>
                                                            <p class="title">
                                                            <ul>
                                                                <li>
                                                                    Date: <?php echo $reservation['date_reservation']; ?></li>
                                                                <li>Heure de
                                                                    début: <?php echo $reservation['heure_debut_reservation']; ?></li>
                                                                <li>Heure de
                                                                    fin: <?php echo $reservation['heure_fin_reservation']; ?></li>
                                                                <li>
                                                                    Statut: <?php echo $reservation['statut']; ?></li>
                                                            </ul>
                                                            </p>
                                                            <br/>
                                                            <!-- Bouton Modifier et annuler réservation -->
                                                            <?php if ($userRole == 'client') : ?>
                                                                <?php if ($reservation['statut'] == ReservationStatusEnum::EN_ATTENTE || $reservation['statut'] == ReservationStatusEnum::RESERVER) : ?>
                                                                    <div class="cart-button">
                                                                        <div class="cart">
                                                                            <!-- Bouton Modifier réservation -->
                                                                            <form action="modifier_reservation.php"
                                                                                  method="post">
                                                                                <input type="hidden"
                                                                                       name="reservation_id"
                                                                                       value="<?php echo $reservation['id']; ?>">
                                                                                <button type="submit"
                                                                                        name="update_reservation"
                                                                                        class="button">
                                                                                    Modifier
                                                                                </button>
                                                                            </form>
                                                                        </div>

                                                                        <!-- Bouton Annuler réservation -->
                                                                        <form action="cancel_reservation.php"
                                                                              method="post">
                                                                            <input type="hidden"
                                                                                   name="reservation_id"
                                                                                   value="<?php echo $reservation['id']; ?>">
                                                                            <button type="submit"
                                                                                    name="cancel_reservation"
                                                                                    class="button">
                                                                                Annuler
                                                                            </button>
                                                                        </form>
                                                                        <div class="clear"></div>
                                                                    </div>
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                        <?php endforeach; ?>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="footer-bottom">
                            <div class="copy">
                                <p>&copy 2024 Cars Online . All rights Reserved</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



</body>
</html>
