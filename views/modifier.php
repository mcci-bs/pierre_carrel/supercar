<?php
session_start();
require_once '../controllers/AuthController.php';
require_once '../controllers/CarController.php';


if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['modifier'])) {
    require_once '../handlers/modifier_handler.php';
}

// Vérifier si l'utilisateur est connecté
if (!isset($_SESSION['token'])) {
    // Rediriger l'utilisateur vers la page de connexion s'il n'est pas connecté
    header("Location: login.php");
    exit();
}
// Extraire l'ID de l'utilisateur à partir de la session
$token = $_SESSION['token'];
$userInfo = AuthController::getUserInfoFromToken($token);
$user_id = $userInfo['id'];

// Vérifier si l'identifiant de la voiture est passé dans l'URL
if (!isset($_GET['car_id'])) {
    // Rediriger l'utilisateur vers la page d'accueil s'il n'a pas sélectionné de voiture
    header("Location: home.php");
    exit();
}

// Récupérer l'identifiant de la voiture depuis l'URL
$car_id = $_GET['car_id'];

// Récupérer les détails de la voiture en fonction de son identifiant
$carDetails = CarController::getCarDetails($car_id);
$caracteristiques = CarController::getCaracteristiquesList();
// Vérifier si la voiture existe
if (!$carDetails) {
    // Rediriger l'utilisateur vers la page d'accueil si la voiture n'existe pas
    header("Location: home.php");
    exit();
}
// Récupérer les anciennes images
$oldImages = $carDetails->getImages();
// Boucler sur les anciennes images si elle existent et générer les balises <input> cachées
foreach ($oldImages as $image) {
    echo '<input type="hidden" name="oldImages[]" value="' . $image . '">';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Modifier information</title>
</head>
<body>
<h2>Editer la voiture</h2>

<h3>Images disponibles</h3>
<?php foreach ($carDetails->getImages() as $key => $image) : ?>
    <img src="<?php echo $image; ?>" alt="image_<?php echo $key + 1; ?>" width="50" height="50">
<?php endforeach; ?>
<br><br>
<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST" enctype="multipart/form-data">
    <input type="hidden" name="car_id" value="<?php echo $car_id; ?>">
    <input type="hidden" name="user_id" value="<?php echo $user_id; ?>">

    <label for="marque">Marque :</label><br>
    <input type="text" id="marque" name="marque" value="<?php echo $carDetails->getMarque(); ?>" required><br>

    <label for="model">Modèle :</label><br>
    <input type="text" id="model" name="model" value="<?php echo $carDetails->getModel(); ?>" required><br>

    <label for="year">Année :</label><br>
    <input type="text" id="year" name="year" value="<?php echo $carDetails->getYear(); ?>" required><br>

    <label for="description">Description :</label><br>
    <input type="text" id="description" name="description" value="<?php echo $carDetails->getDescription(); ?>"
           required><br>

    <label for="color">Couleur :</label><br>
    <input type="text" id="color" name="color" value="<?php echo $carDetails->getColor(); ?>" required><br>

    <label for="price">Prix :</label><br>
    <input type="text" id="price" name="price" value="<?php echo $carDetails->getPrice(); ?>" required><br>

    <label for="consumption">Consommation :</label><br>
    <input type="text" id="consumption" name="consumption" value="<?php echo $carDetails->getConsumption(); ?>"
           required><br>

    <!--    afficher la liste des caractéristiques existantes et les sélectionner par défaut dans le formulaire, s'ils en existe-->
    <label for="caracteristiques">Caractéristiques :</label><br>
    <select id="caracteristiques" name="caracteristiques[]" multiple required>
        <?php foreach ($caracteristiques as $caracteristique) : ?>
            <?php $selected = in_array($caracteristique, $carDetails->getCaracteristiques()) ? 'selected' : ''; ?>
            <option value="<?php echo $caracteristique; ?>" <?php echo $selected; ?>><?php echo $caracteristique; ?></option>
        <?php endforeach; ?>
    </select><br>
    <!--    Récupérer les anciennes images, boucler et générer les balises <input> cachées-->
    <?php foreach ($carDetails->getImages() as $image) : ?>
        <input type="hidden" name="oldImages[]" value="<?php echo $image ?>'">
    <?php endforeach; ?>
    <label for="images">Images :</label><br>
    <input type="file" id="images" name="images[]" multiple><br>
    <br>

    <input type="submit" name="modifier" value="Modifier">
</form>


<p><a href="home.php">Retour à la page d'accueil</a></p>
</body>
</html>
