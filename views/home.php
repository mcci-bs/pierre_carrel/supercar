<?php
session_start();
require_once '../controllers/AuthController.php';
require_once '../controllers/CarController.php';
require_once '../controllers/ReservationController.php';
require_once '../utils/ReservationStatusEnum.php';


// Vérifier si l'utilisateur est connecté
if (!isset($_SESSION['token'])) {
    // Rediriger l'utilisateur vers la page de connexion s'il n'est pas connecté
    header("Location: login.php");
    exit();
}

// Extraire les informations de l'utilisateur à partir du token
$token = $_SESSION['token'];
$userInfo = AuthController::getUserInfoFromToken($token);
$userId = $userInfo['id'];
$userRole = $userInfo['role'];

$cars = CarController::getCarsWithPicture($userRole);
$reservations = ReservationController::getReservations($userId, $userRole);

// informations de l'utilisateur et les informations sur les voitures dans un tableau associatif
$data = [
    'userInfo' => $userInfo,
    'cars' => $cars
];

// Vérifier si le formulaire a été soumis
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['toggle_availability'])) {
    // Récupérer les données du formulaire
    $car_id = $_POST['car_id'];
    $available = $_POST['available'];

    // Appeler la méthode pour activer ou désactiver en fonction de la valeur actuelle de available
    CarController::toggleAvailability($car_id, $available);

    // Rediriger vers la même page pour éviter les re-soumissions de formulaire
    header("Location: " . $_SERVER['PHP_SELF']);
    exit();
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Accueil</title>
    <link href="../resources/css/application.css" rel="stylesheet" type="text/css" media="all"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href='//fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
    <!--slider-->
    <script src="../resources/js/jquery.min.js"></script>
    <script src="../resources/js/script.js" type="text/javascript"></script>
</head>
<body>
<!--Debut Vue client-->
<div class="header-bg">
    <div class="wrap">
        <div class="h-bg">
            <div class="total">
                <div class="header">
                    <div class="box_header_user_menu">
                        <ul class="user_menu">
                            <li class="act first">Bienvenue <?php echo $data['userInfo']['lastName']; ?> </li>
                            <?php if (!$userId) : ?>
                                <li class=""><a href="register.php">
                                        <div class="button-t"><span>S'inscrire</span></div>
                                    </a></li>
                                <li class="last"><a href="login.php">
                                        <div class="button-t"><span>Log in</span></div>
                                    </a></li>
                            <?php else : ?>
                                <li class="last"><a href="logout.php">
                                        <div class="button-t"><span>Se déconnecter</span></div>
                                    </a></li>
                            <?php endif; ?>
                        </ul>
                    </div>

                    <div class="header-bot">
                        <div class="logo">
                            <a href="index.html"><img src="../resources/images/logo.png" alt=""/></a>
                        </div>
                        <div class="search">
                            <input type="text" class="textbox" value="" onfocus="this.value = '';"
                                   onblur="if (this.value == '') {this.value = '';}">
                            <button class="gray-button"><span>Search</span></button>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="menu">
                    <div class="top-nav">
                        <ul>
                            <li class="active"><a href="#">Home</a></li>
                            <li><a href="#">About</a></li>
                            <li><a href="cars.php">Voitures</a></li>
                            <li><a href="show_reservation.php"">
                                <?php $title = ($userRole == 'admin') ? 'Les réservations' : 'Réservations';
                                echo $title; ?></a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="banner-top">
                    <div class="header-bottom">
                        <div class="header_bottom_right_images">
                            <?php if ($userRole == 'client') : ?>
                                <div id="slideshow">
                                    <ul class="slides">
                                        <li><a href="details.html">
                                                <canvas></canvas>
                                                <img src="../resources/images/banner4.jpg"
                                                     alt="Marsa Alam underawter close up"></a></li>
                                        <li><a href="details.html">
                                                <canvas></canvas>
                                                <img src="../resources/images/banner2.jpg"
                                                     alt="Turrimetta Beach - Dawn"></a></li>
                                        <li><a href="details.html">
                                                <canvas></canvas>
                                                <img src="../resources/images/banner3.jpg" alt="Power Station"></a></li>
                                        <li><a href="details.html">
                                                <canvas></canvas>
                                                <img src="../resources/images/banner1.jpg" alt="Colors of Nature"></a>
                                        </li>
                                    </ul>
                                    <span class="arrow previous"></span>
                                    <span class="arrow next"></span>
                                </div>
                            <?php endif; ?>
                            <div class="content-wrapper">
                                <div class="content-top">
                                    <div class="box_wrapper"><h1>Voitures</h1>
                                    </div>
                                    <div class="text">
                                        <?php if ($userRole == 'client') : ?>

                                            <?php for ($i = 0; $i < 3 && $i < count($data['cars']); $i++) : ?>
                                                <?php $car = $data['cars'][$i]; ?>
                                                <?php $images = $car->getImages(); ?>
                                                <?php if (!empty($images)) : ?>
                                                    <?php $randomIndex = array_rand($images); ?>
                                                    <?php $randomImage = $images[$randomIndex]; ?>
                                                <?php endif; ?>
                                                <div class="grid_1_of_3 images_1_of_3">
                                                    <div class="grid_1">
                                                        <a href="details.php?car_id=<?php echo $car->getId(); ?>">
                                                            <?php if (!empty($images)) : ?>
                                                                <img src="<?php echo $randomImage; ?>"
                                                                     title="continue reading" alt="">
                                                            <?php else : ?>
                                                                <img src="../resources/images/banner4.jpg"
                                                                     title="continue reading" alt="">
                                                            <?php endif; ?>
                                                        </a>
                                                        <div class="grid_desc">
                                                            <p class="title"><?php echo $car->getMarque() . ' ' . $car->getModel(); ?></p>
                                                            <br/>
                                                            <p class="title1">
                                                            <ul>
                                                                <li>Année : <?php echo $car->getYear(); ?></li>
                                                                <li>Couleur : <?php echo $car->getColor(); ?></li>
                                                            </ul>
                                                            </p>
                                                            <div class="price" style="height: 19px;">
                                                                <span class="reducedfrom">Prix : Rs <?php echo $car->getPrice(); ?></span>
                                                            </div>
                                                            <div class="cart-button">
                                                                <div class="cart">
                                                                    <!-- Bouton de réservation -->
                                                                    <form action="reservation.php" method="get">
                                                                        <input type="hidden" name="car_id"
                                                                               value="<?php echo $car->getId(); ?>">
                                                                        <input type="hidden" name="user_id"
                                                                               value="<?php echo $userId; ?>">
                                                                        <input type="submit" class="button"
                                                                               value="Réserver">
                                                                    </form>
                                                                </div>
                                                                <a class="button"
                                                                   href="details.php?car_id=<?php echo $car->getId(); ?>">Détails</a>
                                                                <div class="clear"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                            <?php endfor; ?>
                                            <!--Affichage de l'admin-->
                                        <?php else : ?>
                                            <?php for ($i = 0; $i < 3 && $i < count($data['cars']); $i++) : ?>
                                                <?php $car = $data['cars'][$i]; ?>
                                                <?php $images = $car->getImages(); ?>
                                                <?php if (!empty($images)) : ?>
                                                    <?php $randomIndex = array_rand($images); ?>
                                                    <?php $randomImage = $images[$randomIndex]; ?>
                                                <?php endif; ?>
                                                <div class="grid_1_of_3 images_1_of_3">
                                                    <div class="grid_1">
                                                        <a href="details.php?car_id=<?php echo $car->getId(); ?>">
                                                            <?php if (!empty($images)) : ?>
                                                                <img src="<?php echo $randomImage; ?>"
                                                                     title="continue reading" alt="">
                                                            <?php else : ?>
                                                                <img src="../resources/images/banner4.jpg"
                                                                     title="continue reading" alt="">
                                                            <?php endif; ?>
                                                        </a>
                                                        <div class="grid_desc">
                                                            <p class="title"><?php echo $car->getMarque() . ' ' . $car->getModel(); ?></p>
                                                            <p>
                                                            <ul>
                                                                <li>Année : <?php echo $car->getYear(); ?></li>
                                                                <li>Couleur : <?php echo $car->getColor(); ?></li>
                                                            </ul>
                                                            </p>
                                                            <div class="price" style="height: 19px;">
                                                                <span class="reducedfrom">Prix : Rs <?php echo $car->getPrice(); ?></span>
                                                            </div>
                                                            <div class="cart-button">
                                                                <div class="cart">
                                                                    <!-- Bouton de réservation -->
                                                                    <a class="button"
                                                                       href="modifier.php?car_id=<?php echo $car->getId(); ?>"
                                                                       style="padding: 6px">Modifier</a>
                                                                </div>

                                                                <form action="" method="post">
                                                                    <input type="hidden" name="car_id"
                                                                           value="<?php echo $car->getId(); ?>">
                                                                    <input type="hidden" name="available"
                                                                           value="<?php echo $car->getAvailable(); ?>">
                                                                    <button class="button" type="submit"
                                                                            name="toggle_availability"
                                                                            style="padding: 6px">
                                                                        <?php echo $car->getAvailable() ? "Désactiver" : "Activer"; ?>
                                                                    </button>
                                                                </form>


                                                                <div class="clear"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                            <?php endfor; ?>
                                        <?php endif; ?>

                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="header-para">
                            <div class="categories">
                                <div class="list-categories">

                                </div>
                                <div class="box">
                                    <div class="box-heading"><h1><a href="#">Cart:&nbsp;</a></h1></div>
                                    <div class="box-content">
                                        <?php
                                        $title = ($userRole == 'admin') ? 'Les réservations :' : 'Mes réservations :';
                                        echo "<h2>$title</h2>";
                                        ?>

                                        &nbsp;<strong> <a
                                                    href="show_reservation.php"><?php echo count($reservations); ?></a></strong>
                                    </div>
                                </div>
                                <div class="box-title">
                                    <h1><span class="title-icon"></span><a href="#">Branches</a></h1>
                                </div>
                                <div class="section group example">
                                    <div class="col_1_of_2 span_1_of_2">
                                        <a href="test1.html"><img src="../resources/images/pic21.jpg" alt=""/></a>
                                        <a href="test1.html"><img src="../resources/images/pic24.jpg" alt=""/></a>
                                        <a href="test1.html"><img src="../resources/images/pic25.jpg" alt=""/></a>
                                    </div>
                                    <div class="col_1_of_2 span_1_of_2">
                                        <a href="test1.html"><img src="../resources/images/pic22.jpg" alt=""/></a>
                                        <a href="test1.html"><img src="../resources/images/pic23.jpg" alt=""/></a>
                                        <a href="test1.html"><img src="../resources/images/pic26.jpg" alt=""/></a>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="footer-bottom">
                            <div class="copy">
                                <p>&copy 2024 Cars Online . All rights Reserved</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--<h2>Bienvenue --><?php //echo $data['userInfo']['lastName']; ?><!-- ! <a href="logout.php">Se déconnecter</a></h2>-->
<!--<p>Voici vos informations :</p>-->
<!--<ul>-->
<!--    <li>Je suis un : --><?php //echo $data['userInfo']['role']; ?><!--</li>-->
<!--    <li>Nom : --><?php //echo $data['userInfo']['lastName']; ?><!--</li>-->
<!--    <li>Prénom : --><?php //echo $data['userInfo']['firstName']; ?><!--</li>-->
<!--    <li>Téléphone : --><?php //echo $data['userInfo']['phone']; ?><!--</li>-->
<!--    <li>Email : --><?php //echo $data['userInfo']['email']; ?><!--</li>-->
<!--    <li>Expiration du token : --><?php //echo $data['userInfo']['exp']; ?><!--</li>-->
<!--</ul>-->
</body>
</html>
