<?php
session_start();
require_once '../controllers/AuthController.php';
require_once '../controllers/CarController.php';
require_once '../controllers/ReservationController.php';

// Vérifier si l'utilisateur est connecté
if (!isset($_SESSION['token'])) {
    // Rediriger l'utilisateur vers la page de connexion s'il n'est pas connecté
    header("Location: login.php");
    exit();
}
// Extraire l'ID de l'utilisateur à partir de la session
$token = $_SESSION['token'];
$userInfo = AuthController::getUserInfoFromToken($token);
$userId = $userInfo['id'];
$userRole = $userInfo['role'];

$reservations = ReservationController::getReservations($userId, $userRole);

// informations de l'utilisateur et les informations sur les voitures dans un tableau associatif
$data = [
    'userInfo' => $userInfo,
];


// Vérifier si l'identifiant de la voiture est passé dans l'URL
if (!isset($_GET['car_id'])) {
    // Rediriger l'utilisateur vers la page d'accueil s'il n'a pas sélectionné de voiture
    header("Location: home.php");
    exit();
}

// Récupérer l'identifiant de la voiture depuis l'URL
$carId = $_GET['car_id'];

// Récupérer les détails de la voiture en fonction de son identifiant
$carDetails = CarController::getCarDetails($carId);

// Vérifier si la voiture existe
if (!$carDetails) {
    // Rediriger l'utilisateur vers la page d'accueil si la voiture n'existe pas
    header("Location: home.php");
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Détails de la voiture</title>
    <link href="../resources/css/application.css" rel="stylesheet" type="text/css" media="all"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href='//fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
    <!--slider-->
    <script src="../resources/js/jquery.min.js"></script>
    <script src="../resources/js/script.js" type="text/javascript"></script>
</head>
<body>
<div class="header-bg">
    <div class="wrap">
        <div class="h-bg">
            <div class="total">
                <div class="header">
                    <div class="box_header_user_menu">
                        <ul class="user_menu">
                            <li class="act first">Bienvenue <?php echo $data['userInfo']['lastName']; ?> </li>
                            <?php if (!$userId) : ?>
                                <li class=""><a href="register.php">
                                        <div class="button-t"><span>S'inscrire</span></div>
                                    </a></li>
                                <li class="last"><a href="login.php">
                                        <div class="button-t"><span>Log in</span></div>
                                    </a></li>
                            <?php else : ?>
                                <li class="last"><a href="logout.php">
                                        <div class="button-t"><span>Se déconnecter</span></div>
                                    </a></li>
                            <?php endif; ?>
                        </ul>
                    </div>

                    <div class="header-bot">
                        <div class="logo">
                            <a href="index.html"><img src="../resources/images/logo.png" alt=""/></a>
                        </div>
                        <div class="search">
                            <input type="text" class="textbox" value="" onfocus="this.value = '';"
                                   onblur="if (this.value == '') {this.value = '';}">
                            <button class="gray-button"><span>Search</span></button>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="menu">
                    <div class="top-nav">
                        <ul>
                            <li><a href="home.php">Home</a></li>
                            <li><a href="#">About</a></li>
                            <li class="active"><a href="#">Voitures</a></li>
                            <li><a href="show_reservation.php"">
                                <?php $title = ($userRole == 'admin') ? 'Les réservations' : 'Réservations';
                                echo $title; ?></a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="banner-top">
                    <div class="header-bottom">
                        <div class="header_bottom_right_images">
                            <div class="content-wrapper">
                                <div class="content-top">
                                    <div class="box_wrapper"><h1>Détails de la voiture</h1>
                                    </div>
                                    <div class="text">
                                        <br/>
                                        <ul>
                                            <li>Marque : <?php echo $carDetails->getMarque(); ?></li>
                                            <li>Modèle : <?php echo $carDetails->getModel(); ?></li>
                                            <li>Année : <?php echo $carDetails->getYear(); ?></li>
                                            <li>Description : <?php echo $carDetails->getDescription(); ?></li>
                                            <li>Couleur : <?php echo $carDetails->getColor(); ?></li>
                                            <li>Prix : <?php echo $carDetails->getPrice(); ?></li>
                                            <li>Consommation : <?php echo $carDetails->getConsumption(); ?></li>
                                            <li>Caractéristiques :</li>
                                            <ul>
                                                <?php foreach ($carDetails->getCaracteristiques() as $caracteristique) : ?>
                                                    <li><?php echo $caracteristique; ?></li>
                                                <?php endforeach; ?>
                                            </ul>
                                            <br/>
                                            <li>Images :</li>
                                            <ul>
                                                <?php $images = $carDetails->getImages() ?>
                                                    <?php foreach ($images as $key => $image) : ?>
                                                        <li><img src="<?php echo $image; ?>" width="100" height="100"></li>
                                                    <?php endforeach; ?>
                                            </ul>
                                        </ul>
                                        <br/>
                                        <?php if ($userRole == 'client') : ?>
                                            <!-- Bouton de réservation -->
                                            <form action="reservation.php" method="get">
                                                <input type="hidden" name="car_id" value="<?php echo $carId; ?>">
                                                <input type="hidden" name="user_id" value="<?php echo $userId; ?>">
                                                <input class="button" type="submit" value="Réserver cette voiture">
                                            </form>
                                        <?php endif; ?>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="header-para">
                            <div class="categories">
                                <div class="list-categories">

                                </div>
                                <div class="box">
                                    <div class="box-heading"><h1><a href="#">Cart:&nbsp;</a></h1></div>
                                    <div class="box-content">
                                        <?php
                                        $title = ($userRole == 'admin') ? 'Les réservations :' : 'Mes réservations :';
                                        echo "<h2>$title</h2>";
                                        ?>

                                        &nbsp;<strong> <a
                                                    href="show_reservation.php"><?php echo count($reservations); ?></a></strong>
                                    </div>
                                </div>
                                <div class="box-title">
                                    <h1><span class="title-icon"></span><a href="#">Branches</a></h1>
                                </div>
                                <div class="section group example">
                                    <div class="col_1_of_2 span_1_of_2">
                                        <a href="test1.html"><img src="../resources/images/pic21.jpg" alt=""/></a>
                                        <a href="test1.html"><img src="../resources/images/pic24.jpg" alt=""/></a>
                                        <a href="test1.html"><img src="../resources/images/pic25.jpg" alt=""/></a>
                                    </div>
                                    <div class="col_1_of_2 span_1_of_2">
                                        <a href="test1.html"><img src="../resources/images/pic22.jpg" alt=""/></a>
                                        <a href="test1.html"><img src="../resources/images/pic23.jpg" alt=""/></a>
                                        <a href="test1.html"><img src="../resources/images/pic26.jpg" alt=""/></a>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="footer-bottom">
                            <div class="copy">
                                <p>&copy 2024 Cars Online . All rights Reserved</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
