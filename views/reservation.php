<?php
require_once '../controllers/AuthController.php';
require_once '../controllers/CarController.php';
require_once '../controllers/ReservationController.php';

// Inclusion du gestionnaire de réservation
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['reserve'])) {
    require_once '../handlers/reservation_handler.php';
}

// Vérifier si l'utilisateur est connecté
session_start();
if (!isset($_SESSION['token'])) {
    // Rediriger l'utilisateur vers la page de connexion s'il n'est pas connecté
    header("Location: login.php");
    exit();
}
// Extraire l'ID de l'utilisateur à partir de la session
$token = $_SESSION['token'];
$userInfo = AuthController::getUserInfoFromToken($token);
$userId = $userInfo['id'];
$userRole = $userInfo['role'];

$reservations = ReservationController::getReservations($userId, $userRole);
$data = [
    'userInfo' => $userInfo,
];
// Vérifier si l'ID  de l'utilisateur et de la voiture est passé en paramètre d'URL
if (isset($_GET['car_id']) && isset($_GET['user_id'])) {
    $carId = $_GET['car_id'];
    $userId = $_GET['user_id'];
    // Récupérer les détails de la voiture
    $carDetails = CarController::getCarDetails($carId);
} else {
    // Rediriger l'utilisateur vers une page d'erreur si l'ID de la voiture n'est pas spécifié
    header("Location: error.php");
    exit();
}

// Vérifier si la réservation a réussi
if (isset($_GET['success'])) {
    if ($_GET['success'] === 'true') {
        // Afficher un message de réussite si nécessaire
        $successMessage = "Réservation effectuée avec succès.";
    } else {
        // Afficher un message d'erreur si nécessaire
        $errorMessage = "Échec de la réservation. Veuillez réessayer.";
        // Vérifier le type d'erreur et afficher le message approprié
        if (isset($_GET['error'])) {
            if ($_GET['error'] === 'date') {
                $errorMessage = " La date de réservation est antérieure à la date actuelle.";
            } elseif ($_GET['error'] === 'starttime') {
                $errorMessage = " L'heure de debut réservation est antérieure à l'heure actuelle.";
            }elseif ($_GET['error'] === 'endtime') {
                $errorMessage = " L'heure de fin réservation est antérieure à l'heure actuelle.";
            }elseif ($_GET['error'] === 'timeerror') {
                $errorMessage = " La plage d'heure de réservation n'est pas correct.";
            }
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Réservation de voiture</title>
    <link href="../resources/css/application.css" rel="stylesheet" type="text/css" media="all"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href='//fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
    <!--slider-->
    <script src="../resources/js/jquery.min.js"></script>
    <script src="../resources/js/script.js" type="text/javascript"></script>
</head>
<body>


<div class="header-bg">
    <div class="wrap">
        <div class="h-bg">
            <div class="total">
                <div class="header">
                    <div class="box_header_user_menu">
                        <ul class="user_menu">
                            <li class="act first">Bienvenue <?php echo $data['userInfo']['lastName']; ?> </li>
                            <?php if (!$userId) : ?>
                                <li class=""><a href="register.php">
                                        <div class="button-t"><span>S'inscrire</span></div>
                                    </a></li>
                                <li class="last"><a href="login.php">
                                        <div class="button-t"><span>Log in</span></div>
                                    </a></li>
                            <?php else : ?>
                                <li class="last"><a href="logout.php">
                                        <div class="button-t"><span>Se déconnecter</span></div>
                                    </a></li>
                            <?php endif; ?>
                        </ul>
                    </div>

                    <div class="header-bot">
                        <div class="logo">
                            <a href="index.html"><img src="../resources/images/logo.png" alt=""/></a>
                        </div>
                        <div class="search">
                            <input type="text" class="textbox" value="" onfocus="this.value = '';"
                                   onblur="if (this.value == '') {this.value = '';}">
                            <button class="gray-button"><span>Search</span></button>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="menu">
                    <div class="top-nav">
                        <ul>
                            <li><a href="home.php">Home</a></li>
                            <li><a href="about.html">About</a></li>
                            <li><a href="cars.php">Voitures</a></li>
                            <li class="active"><a href="show_reservation.php"">
                                <?php $title = ($userRole == 'admin') ? 'Les réservations' : 'Réservations';
                                echo $title; ?></a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="banner-top">
                    <div class="header-bottom">
                        <div class="header_bottom_right_images">
                            <div class="content-wrapper">
                                <div class="content-top">
                                    <div class="box_wrapper"><h1>Nouvelle réservation</h1>
                                    </div>
                                    <div class="text">
                                        <br/>
                                        <!-- Afficher les messages de succès ou d'erreur ici, s'ils existent -->
                                        <?php if (isset($errorMessage)) : ?><p style="color: red;"><?php echo $errorMessage; ?></p><?php endif; ?>
                                        <?php if (isset($successMessage)) : ?><p style="color: green;"><?php echo $successMessage; ?></p><?php endif; ?>
                                        <br/>
                                        <br/>
                                        <h3>Détails de la voiture :</h3>
                                        <br/>
                                        <p>Marque : <?php echo $carDetails->getMarque(); ?></p>
                                        <p>Modèle : <?php echo $carDetails->getModel(); ?></p>
                                        <p>Année : <?php echo $carDetails->getYear(); ?></p>
                                        <br/>

                                        <h3>Formulaire de réservation :</h3>
                                        <!-- Formulaire de réservation -->
                                        <div>
                                            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                                                <!-- Champ caché pour l'ID de l'utilisateur -->
                                                <input type="hidden" name="user_id" value="<?php echo $userId; ?>">
                                                <!-- Champ caché pour l'ID de la voiture -->
                                                <input type="hidden" name="car_id" value="<?php echo $carId; ?>">

                                                <!-- Champ pour la date -->
                                                <label for="reservation_date">Date de réservation :</label>
                                                <input type="date" id="reservation_date" name="reservation_date" required>

                                                <!-- Champ pour l'heure -->
                                                <label for="reservation_time">Heure debut de réservation :</label>
                                                <input type="time" id="reservation_start_time" name="reservation_start_time" required>

                                                <!-- Champ pour l'heure -->
                                                <label for="reservation_time">Heure fin de réservation :</label>
                                                <input type="time" id="reservation_end_time" name="reservation_end_time" required>

                                                <br/>
                                                <?php if ($userRole == 'client') : ?>
                                                    <!-- Bouton de réservation -->
                                                    <input class="button" type="submit" name="reserve" value="Confirmer la réservation">
                                                <?php endif; ?>

                                            </form>
                                            <br/>
                                            <br/><br/>
                                            <br/>
                                            <p><a class="button" href="details.php?car_id=<?php echo $carId; ?>">Détails de la voiture</a></p>


                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="header-para">
                            <div class="categories">
                                <div class="list-categories">

                                </div>
                                <div class="box">
                                    <div class="box-heading"><h1><a href="#">Cart:&nbsp;</a></h1></div>
                                    <div class="box-content">
                                        <?php
                                        $title = ($userRole == 'admin') ? 'Les réservations :' : 'Mes réservations :';
                                        echo "<h2>$title</h2>";
                                        ?>

                                        &nbsp;<strong> <a
                                                    href="show_reservation.php"><?php echo count($reservations); ?></a></strong>
                                    </div>
                                </div>
                                <div class="box-title">
                                    <h1><span class="title-icon"></span><a href="#">Branches</a></h1>
                                </div>
                                <div class="section group example">
                                    <div class="col_1_of_2 span_1_of_2">
                                        <a href="test1.html"><img src="../resources/images/pic21.jpg" alt=""/></a>
                                        <a href="test1.html"><img src="../resources/images/pic24.jpg" alt=""/></a>
                                        <a href="test1.html"><img src="../resources/images/pic25.jpg" alt=""/></a>
                                    </div>
                                    <div class="col_1_of_2 span_1_of_2">
                                        <a href="test1.html"><img src="../resources/images/pic22.jpg" alt=""/></a>
                                        <a href="test1.html"><img src="../resources/images/pic23.jpg" alt=""/></a>
                                        <a href="test1.html"><img src="../resources/images/pic26.jpg" alt=""/></a>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="footer-bottom">
                            <div class="copy">
                                <p>&copy 2024 Cars Online . All rights Reserved</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
