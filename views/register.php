<?php
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['register'])) {
    require_once '../handlers/register_handler.php';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Google fonts -->
    <link href="//fonts.googleapis.com/css2?family=Kumbh+Sans:wght@300;400;700&display=swap" rel="stylesheet">
    <!-- CSS Stylesheet -->
    <link rel="stylesheet" href="../resources/css/style.css" type="text/css" media="all"/>
    <title>Inscription</title>
</head>



<body>
<?php if (isset($errorMessage)) : ?>
    <p style="color: red;"><?php echo $errorMessage; ?></p>
<?php endif; ?>

<div class="signinform">
    <h1>Inscription</h1>
    <!-- container -->
    <div class="container">
        <!-- main content -->
        <div class="w3l-form-info">
            <div class="w3_info">
                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
                    <div class="input-group">
                        <span><i class="fas fa-user" aria-hidden="true"></i></span>
                        <input type="text" id="username" name="username" placeholder="Nom d'utilisateur" required="">
                    </div>
                    <div class="input-group">
                        <span><i class="fas fa-key" aria-hidden="true"></i></span>
                        <input type="password" id="password" name="password" placeholder="Mot de passe" required="">
                    </div>
                    <div class="input-group">
                        <span><i class="fas fa-user" aria-hidden="true"></i></span>
                        <input type="text" id="firstName" name="firstName" placeholder="Prénom" required="">
                    </div>
                    <div class="input-group">
                        <span><i class="fas fa-user" aria-hidden="true"></i></span>
                        <input type="text" id="lastName" name="lastName" placeholder="Nom" required="">
                    </div>
                    <div class="input-group">
                        <span><i class="fas fa-user" aria-hidden="true"></i></span>
                        <input type="email" id="email" name="email" placeholder="Email" required="">
                    </div>
                    <div class="input-group">
                        <span><i class="fas fa-user" aria-hidden="true"></i></span>
                        <input type="text" id="phone" name="phone" placeholder="Téléphone" required="">
                    </div>
                    <button class="btn btn-primary btn-block" type="submit" name="register">S'inscrire</button>
                </form>
                <p class="account">Déjà inscrit ?<a href="login.php"> Connectez-vous ici</a></p>
            </div>
        </div>
        <!-- //main content -->
    </div>
    <!-- //container -->
    <!-- footer -->
    <div class="footer">
        <p>&copy; 2024 All Rights Reserved</p>
    </div>
    <!-- footer -->
</div>

<!-- fontawesome v5-->
<script src="../resources/js/fontawesome.js"></script>
</body>
</html>
