<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Page non trouvée</title>
</head>
<body>
<h1>Erreur 404 - Page non trouvée</h1>
<p>Désolé, la page que vous recherchez n'a pas été trouvée.</p>
<p>Veuillez revenir à la <a href="/Supercar/views/home.php">page d'accueil</a> ou contactez l'administrateur du site pour obtenir de l'aide.</p>
</body>
</html>
