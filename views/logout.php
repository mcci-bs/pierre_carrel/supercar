<?php
session_start();
require_once '../controllers/AuthController.php';

// Déconnecter l'utilisateur en invalidant le token
AuthController::invalidateToken();

// Détruire toutes les données de session
$_SESSION = array();

// Détruire la session
session_destroy();

// Rediriger l'utilisateur vers la page de connexion
header("Location: login.php");
exit();
?>
